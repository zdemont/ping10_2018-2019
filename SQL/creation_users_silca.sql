/*
* Creation de l'utilisateur ping10_silca_ecr
* Cet utilisateur sera utililé en interne pour mettre à jour les données de la base
* 
*/

-- Suppression de l'utilisateur s'il existe
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ANSI';
USE silca ;
DROP PROCEDURE IF EXISTS silca.drop_user_if_exists ;
DELIMITER $$
CREATE PROCEDURE silca.drop_user_if_exists()
BEGIN
  DECLARE foo BIGINT DEFAULT 0 ;
  SELECT COUNT(*)
  INTO foo
    FROM mysql.user
      WHERE User = 'ping10_silca_ecr' and  Host = '192.168.1.99';
   IF foo > 0 THEN
         DROP USER 'ping10_silca_ecr'@'192.168.1.99';
  END IF;
END ;$$
DELIMITER ;

CALL silca.drop_user_if_exists() ;
DROP PROCEDURE IF EXISTS silca.drop_users_if_exists ;
SET SQL_MODE=@OLD_SQL_MODE ;

-- création du user
CREATE USER 'ping10_silca_ecr'@'192.168.1.99' IDENTIFIED BY 'ping10';
GRANT ALL PRIVILEGES ON silca.* TO 'ping10_silca_ecr'@'192.168.1.99';

/*
*Creation de l'utilisateur ping10_silca_lec
* Cet utilisateur sera utililé par l'appli pour lire des données dans la base
*/
-- Suppression de l'utilisateur s'il existe
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ANSI';
USE silca ;
DROP PROCEDURE IF EXISTS silca.drop_user_if_exists ;
DELIMITER $$
CREATE PROCEDURE silca.drop_user_if_exists()
BEGIN
  DECLARE foo BIGINT DEFAULT 0 ;
  SELECT COUNT(*)
  INTO foo
    FROM mysql.user
      WHERE User = 'ping10_silca_lec' and  Host = 'localhost';
   IF foo > 0 THEN
         DROP USER 'ping10_silca_lec'@'localhost';
  END IF;
END ;$$
DELIMITER ;

CALL silca.drop_user_if_exists() ;
DROP PROCEDURE IF EXISTS silca.drop_users_if_exists ;
SET SQL_MODE=@OLD_SQL_MODE ;

-- création du user
CREATE USER 'ping10_silca_lec'@'localhost' IDENTIFIED BY 'ping10';
GRANT SELECT ON silca.* TO 'ping10_silca_lec'@'localhost';

-- Mise à jour de la table de privilèges
FLUSH PRIVILEGES;
