-- MySQL dump 10.13  Distrib 5.6.41, for Win64 (x86_64)
--
-- Host: localhost    Database: silca
-- ------------------------------------------------------
-- Server version	5.6.41-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `ca_hebdo_chb`
--

LOCK TABLES `ca_hebdo_chb` WRITE;
/*!40000 ALTER TABLE `ca_hebdo_chb` DISABLE KEYS */;
INSERT INTO `ca_hebdo_chb` VALUES ('31115734300023',27,2018,31),('31115734300023',29,2018,32),('31115734300023',30,2018,8),('31115734300023',34,2018,7),('31115734300023',37,2018,21),('31115734300023',38,2018,8),('31115734300023',41,2018,62),('34973487000019',29,2018,160),('34973487000019',31,2018,103),('34973487000019',33,2018,142),('34973487000019',35,2018,109),('34973487000019',37,2018,305),('40867809400018',27,2018,205),('40867809400018',29,2018,80),('40867809400018',30,2018,396),('40867809400018',31,2018,877),('40867809400018',33,2018,1087),('40867809400018',34,2018,204),('40867809400018',35,2018,282),('40867809400018',36,2018,311),('40867809400018',37,2018,569),('40867809400018',38,2018,432),('40867809400018',39,2018,133),('40867809400018',40,2018,243),('40867809400018',41,2018,614),('40867809400018',42,2018,108),('40867809400018',43,2018,140),('52388796700018',28,2018,23),('52388796700018',29,2018,10),('52388796700018',30,2018,15),('52388796700018',31,2018,24),('52388796700018',37,2018,60),('52388796700018',38,2018,52),('52388796700018',40,2018,27),('52388796700018',41,2018,23);
/*!40000 ALTER TABLE `ca_hebdo_chb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ca_mensu_cmn`
--

LOCK TABLES `ca_mensu_cmn` WRITE;
/*!40000 ALTER TABLE `ca_mensu_cmn` DISABLE KEYS */;
INSERT INTO `ca_mensu_cmn` VALUES ('31115734300023',7,2018,62),('31115734300023',8,2018,8),('31115734300023',9,2018,36),('31115734300023',10,2018,62),('34973487000019',7,2018,160),('34973487000019',8,2018,244),('34973487000019',9,2018,415),('40867809400018',7,2018,285),('40867809400018',8,2018,2564),('40867809400018',9,2018,1595),('40867809400018',10,2018,1239),('52388796700018',7,2018,33),('52388796700018',8,2018,39),('52388796700018',9,2018,112),('52388796700018',10,2018,50);
/*!40000 ALTER TABLE `ca_mensu_cmn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `carto_client_crt`
--

LOCK TABLES `carto_client_crt` WRITE;
/*!40000 ALTER TABLE `carto_client_crt` DISABLE KEYS */;
INSERT INTO `carto_client_crt` VALUES ('31115734300023','762540000',1,0,1),('31115734300023','762700000',1,0,1),('31115734300023','763610000',1,1,1),('31115734300023','765630000',1,1,1),('31115734300023','766930000',1,0,1),('34973487000019','270900000',1,0,1),('34973487000019','270920000',1,1,1),('34973487000019','271050000',1,0,1),('34973487000019','275820000',1,1,1),('34973487000019','276380000',1,0,1),('34973487000019','766400102',1,1,1),('40867809400018','760260000',1,0,1),('40867809400018','765450000',1,0,1),('40867809400018','766410000',1,1,1),('52388796700018','763510702',1,0,1);
/*!40000 ALTER TABLE `carto_client_crt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `commercant_cmt`
--

LOCK TABLES `commercant_cmt` WRITE;
/*!40000 ALTER TABLE `commercant_cmt` DISABLE KEYS */;
INSERT INTO `commercant_cmt` VALUES ('31115734300023','retailer',49.546566,0.191422,17,0,80,20,0,0,60,40,0,40,20,20,20,0,0,60,0,20,0,20,0),('34973487000019','retailer2',49.306175,0.950052,136,0,83,17,0,0,17,83,0,67,17,17,-1,0,0,17,17,17,17,33,-1),('40867809400018','retailer3',49.900083,1.080397,178,0,67,33,0,0,0,100,33,33,33,0,1,0,0,33,33,0,0,33,1),('52388796700018','retailer4',49.34128,0.454327,29,0,100,0,0,0,0,100,50,50,0,0,0,0,0,0,0,0,0,100,0);
/*!40000 ALTER TABLE `commercant_cmt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `frequentation_frq`
--

LOCK TABLES `frequentation_frq` WRITE;
/*!40000 ALTER TABLE `frequentation_frq` DISABLE KEYS */;
INSERT INTO `frequentation_frq` VALUES ('31115734300023',2,1),('31115734300023',4,1),('31115734300023',5,1),('31115734300023',6,1),('31115734300023',7,1),('34973487000019',4,1),('34973487000019',7,1),('40867809400018',2,1),('40867809400018',3,1),('40867809400018',4,1),('40867809400018',5,1),('40867809400018',6,1),('40867809400018',7,1),('52388796700018',6,1),('52388796700018',7,1);
/*!40000 ALTER TABLE `frequentation_frq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `histo_event_evt`
--

LOCK TABLES `histo_event_evt` WRITE;
/*!40000 ALTER TABLE `histo_event_evt` DISABLE KEYS */;
INSERT INTO `histo_event_evt` VALUES ('2019-02-06 03:45:09','profil_pfl','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','profil_pfl','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','profil_pfl','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','user_usr','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','user_usr','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','user_usr','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','user_usr','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','user_usr','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','user_usr','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','user_usr','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','user_usr','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','commercant_cmt','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','commercant_cmt','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','commercant_cmt','root2@192.168.1.99','insert'),('2019-02-06 03:45:09','commercant_cmt','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:10','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_hebdo_chb','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:11','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','ca_mensu_cmn','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','frequentation_frq','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:12','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','carto_client_crt','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','reporting_cli_fid','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','reporting_cli_fid','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','reporting_cli_fid','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','reporting_cli_fid','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','reporting_cli_gp','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','reporting_cli_gp','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','reporting_cli_gp','root2@192.168.1.99','insert'),('2019-02-06 03:45:13','reporting_cli_gp','root2@192.168.1.99','insert');
/*!40000 ALTER TABLE `histo_event_evt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `profil_pfl`
--

LOCK TABLES `profil_pfl` WRITE;
/*!40000 ALTER TABLE `profil_pfl` DISABLE KEYS */;
INSERT INTO `profil_pfl` VALUES (1,'admin'),(3,'commercant'),(2,'conseiller');
/*!40000 ALTER TABLE `profil_pfl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reporting_cli_fid`
--

LOCK TABLES `reporting_cli_fid` WRITE;
/*!40000 ALTER TABLE `reporting_cli_fid` DISABLE KEYS */;
INSERT INTO `reporting_cli_fid` VALUES ('31115734300023',100,0,80,20,0,0,60,40,0,40,20,20,20,0,0,60,0,20,0,20,0),('34973487000019',100,0,83,17,0,0,17,83,0,67,17,17,-1,0,0,17,17,17,17,33,-1),('40867809400018',100,0,67,33,0,0,0,100,33,33,33,0,1,0,0,33,33,0,0,33,1),('52388796700018',100,0,100,0,0,0,0,100,50,50,0,0,0,0,0,0,0,0,0,100,0);
/*!40000 ALTER TABLE `reporting_cli_fid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reporting_cli_gp`
--

LOCK TABLES `reporting_cli_gp` WRITE;
/*!40000 ALTER TABLE `reporting_cli_gp` DISABLE KEYS */;
INSERT INTO `reporting_cli_gp` VALUES ('31115734300023',0,80,20,0,0,60,40,0,40,20,20,20,0,0,60,0,20,0,20,0),('34973487000019',0,83,17,0,0,17,83,0,67,17,17,-1,0,0,17,17,17,17,33,-1),('40867809400018',0,67,33,0,0,0,100,33,33,33,0,1,0,0,33,33,0,0,33,1),('52388796700018',0,100,0,0,0,0,100,50,50,0,0,0,0,0,0,0,0,0,100,0);
/*!40000 ALTER TABLE `reporting_cli_gp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_usr`
--

LOCK TABLES `user_usr` WRITE;
/*!40000 ALTER TABLE `user_usr` DISABLE KEYS */;
INSERT INTO `user_usr` VALUES ('admin',1,'$2y$10$IZXTOg3YUqB9a5DTjyEDnuJf86epVjVr2gIwe9w5b1MCT719XkCo6','WAYNE','Bruce','M'),('adviser',2,'$2y$10$IZXTOg3YUqB9a5DTjyEDnuJf86epVjVr2gIwe9w5b1MCT719XkCo6','FOURNET','Paul','Mme'),('c.kent',2,'$2y$10$5HI80ri2keOSjfbKi824n.JC/bqi.gy/.Vshm9hfajhQDCBKdbwXC','KENT','Clark','M'),('k.capitaine',3,'$2y$10$i6aN5E7Oh0TzMIWuNGQuheE6lq9Fzs5xhCOgv82d2I3oXboIB6SQy','CAPITAINE','KRAB','M'),('retailer',3,'$2y$10$IZXTOg3YUqB9a5DTjyEDnuJf86epVjVr2gIwe9w5b1MCT719XkCo6','ZHANG','Demont','M'),('retailer2',3,'$2y$10$IZXTOg3YUqB9a5DTjyEDnuJf86epVjVr2gIwe9w5b1MCT719XkCo6','OBAME EDOU','Chirihan','M'),('retailer3',3,'$2y$10$IZXTOg3YUqB9a5DTjyEDnuJf86epVjVr2gIwe9w5b1MCT719XkCo6','LANTEUIL','Christophe','M'),('retailer4',3,'$2y$10$IZXTOg3YUqB9a5DTjyEDnuJf86epVjVr2gIwe9w5b1MCT719XkCo6','DU','Jiaqian','Mme');
/*!40000 ALTER TABLE `user_usr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-06 17:37:34
