/*
* Triggers ca_hebdo_chb
*/
DROP TRIGGER IF EXISTS after_insert_ca_hebdo_chb;
DELIMITER |
CREATE TRIGGER after_insert_ca_hebdo_chb AFTER INSERT
ON ca_hebdo_chb FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "ca_hebdo_chb";
    SET v_user = CURRENT_USER();
    SET v_type = "insert";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;	

DROP TRIGGER IF EXISTS after_delete_ca_hebdo_chb;
DELIMITER |
CREATE TRIGGER after_delete_ca_hebdo_chb AFTER DELETE
ON ca_hebdo_chb FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "ca_hebdo_chb";
    SET v_user = CURRENT_USER();
    SET v_type = "delete";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

/*
* Triggers ca_mensu_cmn
*/
DROP TRIGGER IF EXISTS after_insert_ca_mensu_cmn;
DELIMITER |
CREATE TRIGGER after_insert_ca_mensu_cmn AFTER INSERT
ON ca_mensu_cmn FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "ca_mensu_cmn";
    SET v_user = CURRENT_USER();
    SET v_type = "insert";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

DROP TRIGGER IF EXISTS after_delete_ca_mensu_cmn;
DELIMITER |
CREATE TRIGGER after_delete_ca_mensu_cmn AFTER DELETE
ON ca_mensu_cmn FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "ca_mensu_cmn";
    SET v_user = CURRENT_USER();
    SET v_type = "delete";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

/*
* Triggers carto_client_crt
*/
DROP TRIGGER IF EXISTS after_insert_carto_client_crt;
DELIMITER |
CREATE TRIGGER after_insert_carto_client_crt AFTER INSERT
ON carto_client_crt FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "carto_client_crt";
    SET v_user = CURRENT_USER();
    SET v_type = "insert";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

DROP TRIGGER IF EXISTS after_delete_carto_client_crt;
DELIMITER |
CREATE TRIGGER after_delete_carto_client_crt AFTER DELETE
ON carto_client_crt FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "carto_client_crt";
    SET v_user = CURRENT_USER();
    SET v_type = "delete";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

/*
* Triggers commercant_cmt
*/
DROP TRIGGER IF EXISTS after_insert_commercant_cmt;
DELIMITER |
CREATE TRIGGER after_insert_commercant_cmt AFTER INSERT
ON commercant_cmt FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "commercant_cmt";
    SET v_user = CURRENT_USER();
    SET v_type = "insert";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

DROP TRIGGER IF EXISTS after_delete_commercant_cmt;
DELIMITER |
CREATE TRIGGER after_delete_commercant_cmt AFTER DELETE
ON commercant_cmt FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "commercant_cmt";
    SET v_user = CURRENT_USER();
    SET v_type = "delete";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

DROP TRIGGER IF EXISTS after_update_commercant_cmt;
DELIMITER |
CREATE TRIGGER after_update_commercant_cmt AFTER UPDATE
ON commercant_cmt FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "commercant_cmt";
    SET v_user = CURRENT_USER();
    SET v_type = "update";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

/*
* Triggers frequentation_frq
*/
DROP TRIGGER IF EXISTS after_insert_frequentation_frq;
DELIMITER |
CREATE TRIGGER after_insert_frequentation_frq AFTER INSERT
ON frequentation_frq FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "frequentation_frq";
    SET v_user = CURRENT_USER();
    SET v_type = "insert";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

DROP TRIGGER IF EXISTS after_delete_frequentation_frq;
DELIMITER |
CREATE TRIGGER after_delete_frequentation_frq AFTER DELETE
ON frequentation_frq FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "frequentation_frq";
    SET v_user = CURRENT_USER();
    SET v_type = "delete";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

/*
* Triggers histo_event_evt
*/
DROP TRIGGER IF EXISTS after_delete_histo_event_evt;
DELIMITER |
CREATE TRIGGER after_delete_histo_event_evt AFTER DELETE
ON histo_event_evt FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "histo_event_evt";
    SET v_user = CURRENT_USER();
    SET v_type = "delete";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

/*
* Triggers profil_pfl
*/
DROP TRIGGER IF EXISTS after_insert_profil_pfl;
DELIMITER |
CREATE TRIGGER after_insert_profil_pfl AFTER INSERT
ON profil_pfl FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "profil_pfl";
    SET v_user = CURRENT_USER();
    SET v_type = "insert";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

DROP TRIGGER IF EXISTS after_delete_profil_pfl;
DELIMITER |
CREATE TRIGGER after_delete_profil_pfl AFTER DELETE
ON profil_pfl FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "profil_pfl";
    SET v_user = CURRENT_USER();
    SET v_type = "delete";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

/*
* Triggers reporting_cli_fid
*/
DROP TRIGGER IF EXISTS after_insert_reporting_cli_fid;
DELIMITER |
CREATE TRIGGER after_insert_reporting_cli_fid AFTER INSERT
ON reporting_cli_fid FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "reporting_cli_fid";
    SET v_user = CURRENT_USER();
    SET v_type = "insert";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

DROP TRIGGER IF EXISTS after_delete_reporting_cli_fid;
DELIMITER |
CREATE TRIGGER after_delete_reporting_cli_fid AFTER DELETE
ON reporting_cli_fid FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "reporting_cli_fid";
    SET v_user = CURRENT_USER();
    SET v_type = "delete";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

/*
* Triggers reporting_cli_gp
*/
DROP TRIGGER IF EXISTS after_insert_reporting_cli_gp;
DELIMITER |
CREATE TRIGGER after_insert_reporting_cli_gp AFTER INSERT
ON reporting_cli_gp FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "reporting_cli_gp";
    SET v_user = CURRENT_USER();
    SET v_type = "insert";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

DROP TRIGGER IF EXISTS after_delete_reporting_cli_gp;
DELIMITER |
CREATE TRIGGER after_delete_reporting_cli_gp AFTER DELETE
ON reporting_cli_gp FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "reporting_cli_gp";
    SET v_user = CURRENT_USER();
    SET v_type = "delete";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

/*
* Triggers user_usr
*/
DROP TRIGGER IF EXISTS after_insert_user_usr;
DELIMITER |
CREATE TRIGGER after_insert_user_usr AFTER INSERT
ON user_usr FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "user_usr";
    SET v_user = CURRENT_USER();
    SET v_type = "insert";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

DROP TRIGGER IF EXISTS after_delete_user_usr;
DELIMITER |
CREATE TRIGGER after_delete_user_usr AFTER DELETE
ON user_usr FOR EACH ROW
BEGIN
	DECLARE v_date DATETIME DEFAULT null; 
    DECLARE v_table, v_user, v_type VARCHAR(45) DEFAULT null;
    
    SET v_date = NOW();
    SET v_table = "user_usr";
    SET v_user = CURRENT_USER();
    SET v_type = "delete";
    
    -- insertion dans la table histo_event_evt
    INSERT INTO histo_event_evt VALUES (v_date,v_table,v_user,v_type);
    
END; |
DELIMITER ;

-- Message de fin
SELECT "Création des triggers de SILCA réussie";
