CREATE DATABASE IF NOT EXISTS `silca` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `silca`;
-- MySQL dump 10.13  Distrib 5.6.41, for Win64 (x86_64)
--
-- Host: localhost    Database: silca
-- ------------------------------------------------------
-- Server version	5.6.41-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ca_hebdo_chb`
--

DROP TABLE IF EXISTS `ca_hebdo_chb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ca_hebdo_chb` (
  `chb_num_siret` varchar(14) NOT NULL COMMENT 'numéro de siret du commerçant dont le CA hebdomadaire est calculé\\n',
  `chb_sem_id` int(2) NOT NULL COMMENT 'numéro de la semaine de l''année\\n',
  `chb_annee` int(4) NOT NULL COMMENT 'année dans laquelle est comprise la semaine',
  `chb_montant_ca_sem` double DEFAULT NULL COMMENT 'chiffre d''affaires de la semaine\\n',
  KEY `fk_chb_num_siret_idx` (`chb_num_siret`),
  CONSTRAINT `fk_chb_num_siret` FOREIGN KEY (`chb_num_siret`) REFERENCES `commercant_cmt` (`cmt_num_siret`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contient une aggrégation du chiffre d''affaires par semaine des commerçants\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ca_mensu_cmn`
--

DROP TABLE IF EXISTS `ca_mensu_cmn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ca_mensu_cmn` (
  `cmn_num_siret` varchar(14) NOT NULL COMMENT 'numéro de siret du commercant\n',
  `cmn_mois_id` int(2) NOT NULL COMMENT 'numéro du mois\\n',
  `cmn_annee` int(4) NOT NULL COMMENT 'année dans laquelle est compris le mois\\n',
  `cmn_montant_ca_mois` double DEFAULT NULL COMMENT 'chiffre d''affaire du mois\n',
  KEY `fk_cmn_num_siret_idx` (`cmn_num_siret`),
  CONSTRAINT `fk_cmn_num_siret` FOREIGN KEY (`cmn_num_siret`) REFERENCES `commercant_cmt` (`cmt_num_siret`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contient une aggrégation du chiffre d''affaires par mois des commerçants';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `carto_client_crt`
--

DROP TABLE IF EXISTS `carto_client_crt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carto_client_crt` (
  `crt_num_siret` varchar(14) NOT NULL COMMENT 'numéro de siret du commercant\n',
  `crt_code_iris` varchar(9) NOT NULL COMMENT 'code iris\n',
  `crt_nbr_cli_total` int(11) DEFAULT NULL COMMENT 'nombre total de clients dans la région associée au code IRIS\n',
  `crt_nbr_cli_gp` int(11) DEFAULT NULL COMMENT 'nombre total de clients fidèles dans la région associée au code IRIS\n',
  `crt_nbr_cli_fidele` int(11) DEFAULT NULL COMMENT 'nombre total de clients gros panier dans la région associée au code IRIS\n',
  KEY `fk_crt_num_siret_idx` (`crt_num_siret`),
  CONSTRAINT `fk_crt_num_siret` FOREIGN KEY (`crt_num_siret`) REFERENCES `commercant_cmt` (`cmt_num_siret`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contient la répartition des clients des commerçants par code IRIS';
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `commercant_cmt`
--

DROP TABLE IF EXISTS `commercant_cmt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commercant_cmt` (
  `cmt_num_siret` varchar(14) NOT NULL COMMENT 'numéro de SIRET du commercant',
  `cmt_usr_id` varchar(45) DEFAULT NULL COMMENT 'id d''utilisateur du commercant',
  `cmt_latitude` double DEFAULT NULL COMMENT 'latitude de l''etablissement du commerçant',
  `cmt_longitude` double DEFAULT NULL COMMENT 'longitude de l''etablissement du commercant',
  `cmt_panier_moy` double DEFAULT NULL COMMENT 'panier moyen des clients du commerçant',
  `cmt_prop_clt_pro` double DEFAULT NULL COMMENT 'proportion de clients professionnels(entreprises)\\n',
  `cmt_prop_clt_h` double DEFAULT NULL COMMENT 'proportion de clients de sexe masculin\n',
  `cmt_prop_clt_f` double DEFAULT NULL COMMENT 'proportion de clients de sexe féminin',
  `cmt_prop_clt_1825` double DEFAULT NULL COMMENT 'proportion de clients appartenant à tranche des 18-25ans\n',
  `cmt_prop_clt_2540` double DEFAULT NULL COMMENT 'proportion de clients appartenant à tranche des 25-40ans\n',
  `cmt_prop_clt_4055` double DEFAULT NULL COMMENT 'proportion de clients appartenant à tranche des 40-55ans\n',
  `cmt_prop_clt_plus55` double DEFAULT NULL COMMENT 'proportion de clients appartenant à tranche des plus de 55 ans\n',
  `cmt_prop_clt_foyer1` double DEFAULT NULL COMMENT 'proportion de clients ayant 1 personne dans leur foyer\n',
  `cmt_prop_clt_foyer2` double DEFAULT NULL COMMENT 'proportion de clients ayant 2 personnes dans leur foyer\n',
  `cmt_prop_clt_foyer3` double DEFAULT NULL COMMENT 'proportion de clients ayant 3 personnes dans leur foyer\n',
  `cmt_prop_clt_foyer4` double DEFAULT NULL COMMENT 'proportion de clients ayant 4 personnes dans leur foyer\n',
  `cmt_prop_clt_foyer_plus5` double DEFAULT NULL COMMENT 'proportion de clients ayant 5 personnes dans leur foyer\n',
  `cmt_prop_clt_csp1` double DEFAULT NULL COMMENT 'proportion de clients appartenant à la catégorie socioprofessionnelle 1\n',
  `cmt_prop_clt_csp2` double DEFAULT NULL COMMENT 'proportion de clients appartenant à la catégorie socioprofessionnelle 2\n',
  `cmt_prop_clt_csp3` double DEFAULT NULL COMMENT 'proportion de clients appartenant à la catégorie socioprofessionnelle 3\n',
  `cmt_prop_clt_csp4` double DEFAULT NULL COMMENT 'proportion de clients appartenant à la catégorie socioprofessionnelle 4\n',
  `cmt_prop_clt_csp5` double DEFAULT NULL COMMENT 'proportion de clients appartenant à la catégorie socioprofessionnelle 5\n',
  `cmt_prop_clt_csp6` double DEFAULT NULL,
  `cmt_prop_clt_csp7` double DEFAULT NULL,
  `cmt_prop_clt_csp8` double DEFAULT NULL,
  PRIMARY KEY (`cmt_num_siret`),
  KEY `fk_cmt_user_id_idx` (`cmt_usr_id`),
  CONSTRAINT `fk_cmt_usr_id` FOREIGN KEY (`cmt_usr_id`) REFERENCES `user_usr` (`usr_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contient les KPI des commerçants';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frequentation_frq`
--

DROP TABLE IF EXISTS `frequentation_frq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frequentation_frq` (
  `frq_num_siret` varchar(14) NOT NULL COMMENT 'numéro de SIRET du commercant\\n',
  `frq_jr_id` int(1) NOT NULL COMMENT 'numéro du jour de la semaine\\n',
  `frq_nbr_moyen_trans` double DEFAULT NULL COMMENT 'nombre moyen de transactions par jour\n',
  KEY `fk_frq_jr_id_idx` (`frq_jr_id`),
  KEY `fk_frq_num_siret_idx` (`frq_num_siret`),
  CONSTRAINT `fk_frq_num_siret` FOREIGN KEY (`frq_num_siret`) REFERENCES `commercant_cmt` (`cmt_num_siret`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contient des informations sur la fréquentation des commerces par jour de la semaine';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `histo_event_evt`
--

DROP TABLE IF EXISTS `histo_event_evt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `histo_event_evt` (
  `evt_date` datetime NOT NULL COMMENT 'date de l''évènement',
  `evt_table` varchar(45) NOT NULL COMMENT 'table concernée par l''évènement',
  `evt_user` varchar(45) NOT NULL COMMENT 'utilisateur à l''origine de l''évènement',
  `evt_type` varchar(45) NOT NULL COMMENT 'type d''évènement'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table contenant un historique des changements effectués sur le table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profil_pfl`
--

DROP TABLE IF EXISTS `profil_pfl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profil_pfl` (
  `pfl_id` int(11) NOT NULL COMMENT 'identifiant du type d''utilisateur(1=>admin,2=>conseiller,3=>commercant)',
  `pfl_libelle` varchar(15) NOT NULL COMMENT 'libellé du type d''utilisateur(admin,conseiller,commercant)',
  PRIMARY KEY (`pfl_id`),
  UNIQUE KEY `pfl_libelle_UNIQUE` (`pfl_libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contient les types de profils utilisateurs';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reporting_cli_fid`
--

DROP TABLE IF EXISTS `reporting_cli_fid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_cli_fid` (
  `fid_num_siret` varchar(14) NOT NULL,
  `fid_prop_clt_total` double DEFAULT NULL COMMENT 'proportion de clients fidèles\\n',
  `fid_prop_clt_pro` double DEFAULT NULL COMMENT 'proportion de clients fidèles professionnels\n',
  `fid_prop_clt_h` double DEFAULT NULL COMMENT 'proportion de clients fidèles de sexe masculin\n',
  `fid_prop_clt_f` double DEFAULT NULL COMMENT 'proportion de clients fidèles de sexe féminin\n',
  `fid_prop_clt_1825` double DEFAULT NULL COMMENT 'proportion de clients fidèles appartenant à la tranche d''âge 18-25ans\n',
  `fid_prop_clt_2540` double DEFAULT NULL COMMENT 'proportion de clients fidèles appartenant à la tranche d''âge 25-40ans\n',
  `fid_prop_clt_4055` double DEFAULT NULL COMMENT 'proportion de clients fidèles appartenant à la tranche d''âge 40-55ans\n',
  `fid_prop_clt_plus55` double DEFAULT NULL COMMENT 'proportion de clients fidèles appartenant à la tranche d''âge 55ans et plus\n',
  `fid_prop_clt_foyer1` double DEFAULT NULL COMMENT 'proportion de clients fidèles ayant 1 personne dans leur foyer\n',
  `fid_prop_clt_foyer2` double DEFAULT NULL COMMENT 'proportion de clients fidèles ayant 2 personnes dans leur foyer\n',
  `fid_prop_clt_foyer3` double DEFAULT NULL COMMENT 'proportion de clients fidèles ayant 3 personnes dans leur foyer\n',
  `fid_prop_clt_foyer4` double DEFAULT NULL COMMENT 'proportion de clients fidèles ayant 4 personnes dans leur foyer\n',
  `fid_prop_clt_foyer_plus5` double DEFAULT NULL COMMENT 'proportion de clients fidèles ayant 5 personnes dans leur foyer\\n',
  `fid_prop_clt_csp1` double DEFAULT NULL COMMENT 'proportion de clients fidèles appartenant à la catégorie socio pro1\n',
  `fid_prop_clt_csp2` double DEFAULT NULL COMMENT 'proportion de clients fidèles appartenant à la catégorie socio pro2\n',
  `fid_prop_clt_csp3` double DEFAULT NULL COMMENT 'proportion de clients fidèles appartenant à la catégorie socio pro3\n',
  `fid_prop_clt_csp4` double DEFAULT NULL COMMENT 'proportion de clients fidèles appartenant à la catégorie socio pro4\n',
  `fid_prop_clt_csp5` double DEFAULT NULL COMMENT 'proportion de clients fidèles appartenant à la catégorie socio pro5\n',
  `fid_prop_clt_csp6` double DEFAULT NULL,
  `fid_prop_clt_csp7` double DEFAULT NULL,
  `fid_prop_clt_csp8` double DEFAULT NULL,
  PRIMARY KEY (`fid_num_siret`),
  CONSTRAINT `fk_fid_num_siret` FOREIGN KEY (`fid_num_siret`) REFERENCES `commercant_cmt` (`cmt_num_siret`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contient pour chaque commerçant les KPI de la clientèle fidèle\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reporting_cli_gp`
--

DROP TABLE IF EXISTS `reporting_cli_gp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_cli_gp` (
  `gp_num_siret` varchar(14) NOT NULL,
  `gp_prop_clt_pro` double DEFAULT NULL COMMENT 'proportion de clients fidèles professionnels\n',
  `gp_prop_clt_h` double DEFAULT NULL COMMENT 'proportion de clients gros panier de sexe masculin\n',
  `gp_prop_clt_f` double DEFAULT NULL COMMENT 'proportion de clients gros panier de sexe féminin\n',
  `gp_prop_clt_1825` double DEFAULT NULL COMMENT 'proportion de clients gros panier appartenant à la tranche d''âge 18-25ans\n',
  `gp_prop_clt_2540` double DEFAULT NULL COMMENT 'proportion de clients gros panier appartenant à la tranche d''âge 25-40ans\n',
  `gp_prop_clt_4055` double DEFAULT NULL COMMENT 'proportion de clients gros panier appartenant à la tranche d''âge 40-55ans\n',
  `gp_prop_clt_plus55` double DEFAULT NULL COMMENT 'proportion de clients gros panier appartenant à la tranche d''âge 55ans et plus\n',
  `gp_prop_clt_foyer1` double DEFAULT NULL COMMENT 'proportion de clients gros panier ayant 1 personne dans leur foyer\n',
  `gp_prop_clt_foyer2` double DEFAULT NULL COMMENT 'proportion de clients gros panier ayant 2 personnes dans leur foyer\n',
  `gp_prop_clt_foyer3` double DEFAULT NULL COMMENT 'proportion de clients gros panier ayant 3 personnes dans leur foyer\n',
  `gp_prop_clt_foyer4` double DEFAULT NULL COMMENT 'proportion de clients gros panier ayant 4 personnes dans leur foyer\n',
  `gp_prop_clt_foyer_plus5` double DEFAULT NULL COMMENT 'proportion de clients gros panier ayant 5 personnes dans leur foyer\\n',
  `gp_prop_clt_csp1` double DEFAULT NULL COMMENT 'proportion de clients gros panier appartenant à la catégorie socio pro1\n',
  `gp_prop_clt_csp2` double DEFAULT NULL COMMENT 'proportion de clients gros panier appartenant à la catégorie socio pro2\n',
  `gp_prop_clt_csp3` double DEFAULT NULL COMMENT 'proportion de clients gros panier appartenant à la catégorie socio pro3\n',
  `gp_prop_clt_csp4` double DEFAULT NULL COMMENT 'proportion de clients gros panier appartenant à la catégorie socio pro4\n',
  `gp_prop_clt_csp5` double DEFAULT NULL COMMENT 'proportion de clients gros panier appartenant à la catégorie socio pro5\n',
  `gp_prop_clt_csp6` double DEFAULT NULL,
  `gp_prop_clt_csp7` double DEFAULT NULL,
  `gp_prop_clt_csp8` double DEFAULT NULL,
  PRIMARY KEY (`gp_num_siret`),
  CONSTRAINT `fk_gp_num_siret` FOREIGN KEY (`gp_num_siret`) REFERENCES `commercant_cmt` (`cmt_num_siret`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contient pour chaque commerçant les KPI de la clientèle gros panier\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_usr`
--

DROP TABLE IF EXISTS `user_usr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_usr` (
  `usr_id` varchar(45) NOT NULL COMMENT 'identifiant de l''utilisateur',
  `usr_pfl_id` int(11) NOT NULL COMMENT 'id du type de profil de l''utilisateur(1=>admin,2=>conseiller,3=>commercant)\n',
  `usr_password` varchar(100) NOT NULL COMMENT 'password de l''utilisateur\n',
  `usr_nom` varchar(45) NOT NULL COMMENT 'nom de l''utilisateur\n',
  `usr_prenom` varchar(45) NOT NULL COMMENT 'prenom de l''utilisateur\n',
  `usr_civilite` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`usr_id`),
  KEY `fk_pfl_id_idx` (`usr_pfl_id`),
  CONSTRAINT `fk_pfl_id` FOREIGN KEY (`usr_pfl_id`) REFERENCES `profil_pfl` (`pfl_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contient les informations de connexion des users';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-29 22:23:51

-- Message de fin
SELECT "Création de SILCA réussie";
