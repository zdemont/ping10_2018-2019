<?php
require ('../controllers/verif_authentification.php');
?>
<!DOCTYPE html>
<html lang="en">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Vos Reportings</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="assets/vendors/icheck/skins/all.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />

    <!--hightcharts scripts-->
    <script src="assets/highcharts/highcharts.js"></script>
    <script src="assets/highcharts/exporting.js"></script>
    <script src="assets/highcharts/export-data.js"></script>
    <script src="assets/highcharts/drilldown.js"></script>
    <link rel="stylesheet" href="assets/css/bot.css">
    <script src="assets/jquery/jquery.min.js"></script>
<style type="text/css">

@media print {
 
   body, article {
      width: 100%;
   }
 
 
   @page {
      margin: 1.15cm;
   }
   }
    
 .A4 {        
    page-break-before: auto;        
    page-break-after: always;
}

</style>
  </head>

<body>
      <!--Button for printing-->
    <br>
    <button class="btn btn-primary btn-fw" type="button" onclick="doPrint()"
            style="margin-left: 10px; margin-right: 10px;"> Imprimer mes reportings!
    </button>
      <!-- partial -->
    <br><br>
      <div class="main-panel">
        <div class="content-wrapper ">
<!--startprint--> <!-- necessary for button printing-->
<div class="A4">
          <div class="d-flex justify-content-center grid-margin">
            <h2>Mes chiffres clés</h2>
          </div>
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin">
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-currency-eur text-danger icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Chiffre d'affaires du mois précédent en €</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" id="LastCA"> </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-trending-up text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Evolution du CA</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" id="EvolCA"> </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin">
            </div>
          </div>

          <div class="d-flex justify-content-center grid-margin">
              <h2>Données de fréquentation</h2>
          </div>
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div id="container" class="d-flex"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="d-flex justify-content-center grid-margin">
              <h2>Chiffre d'affaires par semaines en € </h2>
          </div>
          <div class="row grid-margin stretch-card">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div id="container1" class="h-100"></div>
                </div>
              </div>
            </div>
          </div>
    <!-- ends page 1--> 
</div>
                      
<div class="A4">
    <br><br>
      <div class="d-flex justify-content-center grid-margin">
            <h2>Mes profilings</h2>
        </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 grid-margin stretch-card">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="clearfix">
                                <div class="float-left">
                                    <i class="mdi mdi-shopping text-danger icon-lg"></i>
                                </div>
                                <div class="float-right">
                                    <p class="mb-0 text-right">Panier Moyen</p>
                                    <div class="fluid-container">
                                        <h3 class="font-weight-medium text-right mb-0" id="paniermoy"></h3>
                                    </div>
                                </div>
                            </div>
                            <p class="text-muted mt-3 mb-0">
                                <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Sur les 3 derniers mois
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 grid-margin stretch-card">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="clearfix">
                                <div class="float-left">
                                    <i class="mdi mdi-account-star text-warning icon-lg"></i>
                                </div>
                                <div class="float-right">
                                    <p class="mb-0 text-right">Clients fidèles</p>
                                    <div class="fluid-container">
                                        <h3 class="font-weight-medium text-right mb-0" id="cli_fid_total"></h3>
                                    </div>
                                </div>
                            </div>
                            <p class="text-muted mt-3 mb-0">
                                <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Sur l'année 2018
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 grid-margin stretch-card">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="clearfix">
                                <div class="float-left">
                                    <i class="mdi mdi-briefcase text-success icon-lg"></i>
                                </div>
                                <div class="float-right">
                                    <p class="mb-0 text-right">Clients pro</p>
                                    <div class="fluid-container">
                                        <h3 class="font-weight-medium text-right mb-0" id="prop_cli_pro"></h3>
                                    </div>
                                </div>
                            </div>
                            <p class="text-muted mt-3 mb-0">
                                <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Sur l'année 2018
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        <div class="d-flex justify-content-center grid-margin">
            <h2>Profiling de mes clients total </h2>
        </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container2" class="h-100"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container3" class="h-100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container4" class="h-100"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container5" class="h-100"></div>
                        </div>
                    </div>
                </div>
            </div>
   <!-- page 2 -->
   </div>
          <br><br><br>
            
<div class="A4">
    <div class="d-flex justify-content-center grid-margin">
            <h2>Profiling de mes clients fidèles </h2>
        </div>
 
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container6" class="h-100"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container7" class="h-100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container8" class="h-100"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container9" class="h-100"></div>
                        </div>
                    </div>
                </div>
            </div>
<!-- page 3 -->            
</div>
            <br><br><br>
<div class="A4">
    <div class="d-flex justify-content-center grid-margin">
            <h2>Profiling de mes clients Gros Paniers </h2>
        </div>
 
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container10" class="h-100"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container11" class="h-100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container12" class="h-100"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container13" class="h-100"></div>
                        </div>
                    </div>
                </div>
            </div>
<!-- page 4 -->            
</div>
   <!-- content-swapper ends-->
   </div>
   <!-- main-panel ends -->
   </div>
<!--endprint-->

  <!-- plugins:js -->
  <script src="assets/vendors/js/vendor.bundle.base.js"></script>
  <script src="assets/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="assets/js/off-canvas.js"></script>
  <script src="assets/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="assets/js/dashboard_kpi.js"></script>
  <script src="assets/js/charts/grapheFrequentation.js"></script>
  <script src="assets/js/charts/grapheCA.js"></script>
  <script src="assets/js/printProfiling.js"></script>
  <script>
      getLastCA();
      getEvolCA();
    //Appel CA par semanine
      printGraphCAHebdo();
    //Appel des fonctions du script graphProfiling.js
      getPanierMoy();
      getCliFidele();
      getClipro();
      profilingTotal();
      profilingFidele();
      profilingGrosPanier();
      
  </script>
  <script src="assets/js/charts/setoption.js"></script>
  <!-- End custom js for this page-->

  
<script type="text/javascript">   
  function doPrint() {   
    bdhtml=window.document.body.innerHTML; 
    sprnstr="<!--startprint-->";   
    eprnstr="<!--endprint-->";   
    prnhtml=bdhtml.substr(bdhtml.indexOf(sprnstr)+17);   
    prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));  
    window.document.body.innerHTML = prnhtml;
    window.print(); 
    window.document.body.innerHTML = bdhtml;
}   
</script>

</body>

</html>