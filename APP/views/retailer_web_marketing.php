<?php
require ('../controllers/verif_authentification.php');
?>
<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ma présence sur le web</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="assets/vendors/icheck/skins/all.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />

    <link rel="stylesheet" href="assets/css/bot.css">
    
</head>

<body>
    <!-- Intégration de l'header -->
    <?php 
    if (isset($_SESSION['userAdviser'])){
        include "header_conseiller.php";
    }
    elseif ($_SESSION['userRetailer']){
        include "header_retailer.php";
    }
    ?>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
        <!-- Intégration sidebar -->
            <?php include "sidebar.php"?>
            <div class="main-panel">
                <div class="content-wrapper">
                    <div id="form_web_marketing">
                        <h1>Évaluons votre présence sur internet !</h1>
                    <br><br>
                    <form method="post">
                    <h3 style=" text-decoration: underline;">Ma présence sur les réseaux sociaux</h3>
                    <br>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="rs_q1" style="font-size:20;"><b>Votre établissement possède-t-il une page facebook ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="rs_q1" name='q1' class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="rs_q2" style="font-size:20;"><b>Votre établissement possède-t-il un compte twitter ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="rs_q2" name='q1' class="form-control" type="checkbox"></div>
                    </div>
                    <h3 style=" text-decoration: underline;">Mon identification Google Maps</h3>
                    <br>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="gm_q1" style="font-size:20;"><b>Votre établissement est-il référencé sur google maps ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="gm_q1" name="q2" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="gm_q2" style="font-size:20;"><b>Des photos y sont-elles disponibles ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="gm_q2" name="q2" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="gm_q3" style="font-size:20;"><b>Des avis (positifs) y sont-ils visibles ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="gm_q3" name="q3" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="gm_q4" style="font-size:20;"><b>Peut-on y voir mes horaires d'ouverture ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="gm_q4" name="q3" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="gm_q5" style="font-size:20;"><b>Mon numéro de téléphone est-il disponible ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="gm_q5" name="q3" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="gm_q6" style="font-size:20;"><b>Y a-t-il un lien vers mon site web ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="gm_q6" name="q3" class="form-control" type="checkbox"></div>
                    </div>
                    <h3 style=" text-decoration: underline;">Mon site web</h3>
                    <br>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="sw_q1" style="font-size:20;"><b>Mon site est-il visité par un nombre significatif de clients par rapport à ma clientèle totale ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="sw_q1" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="sw_q2" style="font-size:20;"><b>Mon site est-il adapté aux petits écrans des smartphones ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="sw_q2" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="sw_q3" style="font-size:20;"><b>Mon site possède-t-il une vitesse de chargement raisonnable selon un service externe d'évaluation (par exemple GTmetrix) ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="sw_q3" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="sw_q4" style="font-size:20;"><b>Le contenu de mon site est-il mis à jour fréquemment (nouveau contenu tous les mois) ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="sw_q4" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <h3 style=" text-decoration: underline;">Le référencement de mon site web</h3>
                    <h6>à remplir de préférence avec votre webmaster</h6>
                    <br>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="rf_q1" style="font-size:20;"><b>L'adresse de mon site fonctionne-t-elle avec et sans le préfixe 'www.' ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="rf_q1" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="rf_q2" style="font-size:20;"><b>Les URLs de mon site sont propres (pas de requête ou de caractères spéciaux) ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="rf_q2" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="rf_q3" style="font-size:20;"><b>L'adresse IP de mon site pointe-t-elle bien vers mon site, et toutes les redirections sont bien organisées ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="rf_q3" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="rf_q4" style="font-size:20;"><b>Mon site possède un fichier 'sitemap.xml' et un fichier 'robots.txt' pour aider les moteurs de recherche à le trouver ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="rf_q4" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="rf_q5" style="font-size:20;"><b>Aucun contenu dupliqué ne se trouve sur mon site (peu apprécié des moteurs de recherches) ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="rf_q5" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="rf_q6" style="font-size:20;"><b>Toutes mes images possèdent-elles une balise 'alt' ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="rf_q6" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="rf_q7" style="font-size:20;"><b>Mon site a été créé en respect des normes W3C ?</b></label></div>
                        <div class="col-sm-2 input-column"><input id="rf_q7" name="q4" class="form-control" type="checkbox"></div>
                    </div>
                    <br><br>
                    <div class="form-row form-group">
                        <div class="col-sm-6 label-column"><label for="confirm" style="font-size:20;"><b>Je confirme avoir rempli l'intégralité des champs de ce questionnaire au meilleur de mes connaissances.</b></label></div>
                        <div class="col-sm-2 input-column"><input id="confirm" name="q4" class="form-control" type="checkbox" required></div>
                    </div>
                    <button class="btn submit-button" role="button" name="evaluation" href="" data-toggle="modal" onclick="grade();">Obtenir une évaluation de mon web-marketing.</button>
                    </div>
                    <br><br><br>
                    </form>
                </div>
            </div>
        <div class="modal fade" role="dialog" tabindex="-1" id="modal_help" data-toggle="modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Évalutation de votre présence sur internet.</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                <div class="modal-body" id="custom_content">
                   
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Close</button></div>
            </div>
        </div>
    </div>

    <script src="assets/js/form_marketing.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    <?php include "footer_standard.php"?>

</body>

</html>