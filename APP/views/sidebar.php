<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li>
        <h4 style="text-align: center">Menu de navigation</h4>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="retailer_start.php">
            <i class="menu-icon mdi mdi-home"></i>
            <span class="menu-title" >Accueil</span>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="retailer_dashboard.php">
            <i class="menu-icon mdi mdi-chart-bar"></i>
            <span class="menu-title" >Analyser mon activité</span>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="retailer_profiling.php">
            <i class="menu-icon mdi mdi-account-multiple-outline"></i>
            <span class="menu-title">Connaître mes clients</span>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="retailer_cartographie.php">
            <i class="menu-icon mdi mdi-map"></i>
            <span class="menu-title">Localiser mes clients</span>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="retailer_web_marketing.php">
            <i class="menu-icon mdi mdi-web"></i>
            <span class="menu-title">Ma présence sur le web</span>
        </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="retailer_print_charts.php" target="_blank">
                <i class="menu-icon mdi mdi-printer"></i>
                <span class="menu-title" >Imprimer mon reporting </span>
            </a>
        </li>
    </ul>
</nav>