function grade()
{
    $(".modal-body").html("");

    var grade1 = 0;
    var grade2 = 0;
    var grade3 = 0;
    var grade4 = 0;

    if(document.getElementById("rs_q1").checked)grade1++;
    if(document.getElementById("rs_q2").checked)grade1++;
    if(document.getElementById("gm_q1").checked)grade2++;
    if(document.getElementById("gm_q2").checked)grade2++;
    if(document.getElementById("gm_q3").checked)grade2++;
    if(document.getElementById("gm_q4").checked)grade2++;
    if(document.getElementById("gm_q5").checked)grade2++;
    if(document.getElementById("gm_q6").checked)grade2++;
    if(document.getElementById("sw_q1").checked)grade3++;
    if(document.getElementById("sw_q2").checked)grade3++;
    if(document.getElementById("sw_q3").checked)grade3++;
    if(document.getElementById("sw_q4").checked)grade3++;
    if(document.getElementById("rf_q1").checked)grade4++;
    if(document.getElementById("rf_q2").checked)grade4++;
    if(document.getElementById("rf_q3").checked)grade4++;
    if(document.getElementById("rf_q4").checked)grade4++;
    if(document.getElementById("rf_q5").checked)grade4++;
    if(document.getElementById("rf_q6").checked)grade4++;
    if(document.getElementById("rf_q7").checked)grade4++;

    var com1;
    var com2;
    var com3;
    var com4;

    switch(grade1){
        case 0 :
            com1 = document.createTextNode("Vous n'avez pas de présence sur les réseaux sociaux, celle-ci ne vous apporterait pas vraiment davantage de visibilité client, car ils sont mals référencés, mais vous permettrait de suivre d'autres acteurs de la vie commerciale, telle que votre commune, ou la chambre de commerce et d'industrie.");
        break;
        case 1 :
            com1 = document.createTextNode("Les réseaux sociaux sont un des moyens les plus faciles d'avoir une présence sur internet, pensez à en couvrir davantage.");
        break;
        case 2:
            com1 = document.createTextNode("C'est parfait, pensez à partager du contenu régulièrement !");
        break;
    }

    switch(true){
        case (grade2 == 0) :
            com2 = document.createTextNode("Google Maps est un incontournable de la présence sur internet, il est aussi gratuit. Pensez-y !");
        break;
        case (grade2 < 6) :
            com2 = document.createTextNode("N'hésitez pas à étoffer votre profil Google Maps, c'est souvent la première impression sur votre établissement.");
        break;
        case (grade2 > 5) :
            com2 = document.createTextNode("Votre profil Google Maps est bien référencé. Félicitations !");
        break;
    }

    switch(true){
        case (grade3 == 0) :
            com3 = document.createTextNode("Pensez à réfléchir à une vitrine web. Une réalisation professionnelle raisonnable coûte 1000 euros pour les projets les plus simples (site vitrines), et jusqu'à 3000 pour les plus développés.");
        break;
        case (grade3 < 4) :
            com3 = document.createTextNode("Votre site web est un bel effort, n'hésitez pas à développer cette solution si vous le pensez pertinents.");
        break;
        case (grade3 > 3) :
            com3 = document.createTextNode("Votre site web est de qualité et adapté à une performance professionnelle.");
        break;
    }

    switch(true){
        case (grade4 == 0) :
            com4 = document.createTextNode("Votre site web a besoin d'améliorations pour être facilement trouvé par les moteurs de recherches, n'hésitez pas à demander un devis gratuit à un professionnel du référencement.");
        break;
        case (grade4 < 7) :
            com4 = document.createTextNode("Votre site web peut être amélioré mais il s'agit d'une bonne base. N'hésitez pas à demander un devis gratuit de référencement à un professionnel.");
        break;
        case (grade4 > 6) :
            com4 = document.createTextNode("Votre site web devrait être très bien référencé, vous avez souscrit à un prestateur de qualité.");
        break;
    }

    var para = document.createElement("p");
    var title1 = document.createElement("h6");
    var label1 = document.createTextNode("Ma note sur les réseaux sociaux : " + grade1 + "/2");
    var title2 = document.createElement("h6");
    var label2 = document.createTextNode("Ma note sur google maps : " + grade2 + "/6");
    var title3 = document.createElement("h6");
    var label3 = document.createTextNode("Ma note sur mon site web : " + grade3 + "/4");
    var title4 = document.createElement("h6");
    var label4 = document.createTextNode("Ma note sur mon référencement : " + grade4 + "/7");
    
    title1.appendChild(label1);
    title2.appendChild(label2);
    title3.appendChild(label3);
    title4.appendChild(label4);

    para.appendChild(title1);
    para.append(com1);
    para.appendChild(document.createElement("br"));
    para.appendChild(document.createElement("br"));
    para.appendChild(title2);
    para.append(com2);
    para.appendChild(document.createElement("br"));
    para.appendChild(document.createElement("br"));
    para.appendChild(title3);
    para.append(com3);
    para.appendChild(document.createElement("br"));
    para.appendChild(document.createElement("br"));
    para.appendChild(title4);
    para.append(com4);

    var element = document.getElementById("custom_content");
    element.appendChild(para);


    if(document.getElementById("confirm").checked){
        $('#modal_help').modal('show');
    }
}
