// url du controleurs php chargé de préparer les données à envoyer aux graphes
var url = "../controllers/link_retailer_dashboard.php?func=";

/*Cette fonction a deux rôles:
    -récupérer les données en faisant appel au controleur
    -afficher le graphe
*/
function printGraphCAMens() {
    $.get(url + "camensuel",
        success = function (data) {
            //console.log(data);
            ChartDataMens(data);
        }, "json")
};
printGraphCAMens();// appel de la fonction pour que le graphe soit affiché au chargement de la page

// Traçage du graphe sur le CA mensuel
function ChartDataMens(data) {
    var chart1 = Highcharts.chart('container1', {
        chart: {
            type: 'column'
        },
        title: {
            text: "Chiffre d'Affaires par mois pour l'année " + data.annee +" en €"
        },
        colors: ['#006745'],
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: "Chiffre d'Affaires "
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Chiffre Affaire: <b>{point.y:.1f} €</b>'
        },
        series: [{
            name: 'Mois',
            data: data.moisReporting,
            dataLabels: {
                enabled: false,
                rotation: 0,
                color: '#008B93',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
};

//fonction pour récupérer les données du graphique Chiffre d'Affaire Hebdomadaire en effectuant un appel au controleur
function printGraphCAHebdo() {
    $.get(url + "cahebdo",
        success = function (data) {
            // console.log(data);
            ChartDataHebdo(data);
        }, "json")
};

// Traçage du graphe sur le CA hebdomadaire
function ChartDataHebdo(data) {
    var chart2 = Highcharts.chart('container1', {
        chart: {
            scrollablePlotArea: {
                minWidth: 700
            }
        },
        colors: ['#006745'],
        title: {
            text: "Chiffre d'Affaires par semaine pour l'année " + data.annee +" en €"
        },

        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Chiffre d Affaire '
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Chiffre Affaire: <b>{point.y:.1f} €</b>'
        },
        series: [{
            name: 'Semaines',
            data: data.semaineReporting,

            dataLabels: {
                enabled: false,
                rotation: -90,
                color: '#008B93',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
};
