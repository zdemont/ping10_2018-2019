function profilingTotal() {
    getFoyerGraphForAll();
    getAgeGraphForAll();
    getCSPGraphForAll();
    getHFGraphForAll();
};

function profilingFidele() {
    getAgeGraphForFidele();
    getFoyerGraphForFidele();
    getCSPGraphForFidele();
    getHFGraphForFidele();
};

function profilingGrosPanier() {
    getAgeGraphForGP();
    getFoyerGraphForGP();
    getCSPGraphForGP();
    getHFGraphForGP();
};

function getPanierMoy() {
    var url = "../controllers/link_profiling.php?donnee=PanierMoy";
    $.get(url, success=function(data){
        //traitement de donnee met ici
        document.getElementById("paniermoy").innerHTML = data+" €";
    });
};

function getCliFidele() {
    var url = "../controllers/link_profiling.php?donnee=clifidele";
    $.get(url, success=function(data){
        //traitement de donnee met ici
        document.getElementById("cli_fid_total").innerHTML = data+" %";
    });
};

function getClipro() {
    var url = "../controllers/link_profiling.php?donnee=clipro";
    $.get(url, success=function(data){
        //traitement de donnee met ici
        document.getElementById("prop_cli_pro").innerHTML = data+" %";
    });
};

function getFoyerGraphForAll() {
    var url = "../controllers/link_profiling.php?donnee=foyerall";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawFoyerGraph(data[0], data[1], data[2], data[3], data[4]);
    }, dataType = "json");
}


function getFoyerGraphForGP() {
    var url = "../controllers/link_profiling.php?donnee=foyerGP";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawFoyerGraph(data[0], data[1], data[2], data[3], data[4]);
    }, dataType = "json");
}


function getFoyerGraphForFidele() {
    var url = "../controllers/link_profiling.php?donnee=foyerfidele";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawFoyerGraph(data[0], data[1], data[2], data[3], data[4]);
    }, dataType = "json");
}


function getAgeGraphForAll() {
    var url = "../controllers/link_profiling.php?donnee=Ageall";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawAgeGraph(data[0], data[1], data[2], data[3]);
    }, dataType = "json");
}


function getAgeGraphForGP() {
    var url = "../controllers/link_profiling.php?donnee=AgeGP";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawAgeGraph(data[0], data[1], data[2], data[3]);
    }, dataType = "json");
}


 function getAgeGraphForFidele() {
     var url = "../controllers/link_profiling.php?donnee=Agefidele";
     $.get(url, success = function (data) {
         //traitement de donnee met ici
         drawAgeGraph(data[0], data[1], data[2], data[3]);
     }, dataType = "json");
 }

function drawFoyerGraph(prop_foyer1, prop_foyer2, prop_foyer3, prop_foyer4, prop_foyer5plus) {
    var label1 = "1 personne";
    var label2 = "2 personnes";
    var label3 = "3 personnes";
    var label4 = "4 personnes";
    var label5 = "5 personnes et plus";
    if(prop_foyer1<=0){prop_foyer1=null;}
    if(prop_foyer2<=0)prop_foyer2=null;
    if(prop_foyer3<=0)prop_foyer3=null;
    if(prop_foyer4<=0)prop_foyer4=null;
    if(prop_foyer5plus<=0)prop_foyer5plus=null;

    Highcharts.chart('container4', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Nombres de personnes dans le foyer'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Foyers',
            colorByPoint: true,
            data: [{
                name: label1,
                y: prop_foyer1
            }, {
                name: label2,
                y: prop_foyer2
            }, {
                name: label3,
                y: prop_foyer3
            }, {
                name: label4,
                y: prop_foyer4
            }, {
                name: label5,
                y: prop_foyer5plus
            }]
        }],
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ['printChart', 'separator', 'downloadPDF', 'downloadPNG', 'downloadSVG', 'separator', 'downloadXLS']
                }
            }
        }
    });
}

function drawAgeGraph(prop_age1825, prop_age2540, prop_age4055, prop_age55plus) {

    if(prop_age1825<=0){prop_age1825=null;}
    if(prop_age2540<=0){prop_age2540=null;}
    if(prop_age4055<=0){prop_age4055=null;}
    if(prop_age55plus<=0){prop_age55plus=null;}

    Highcharts.chart('container2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Age de vos clients'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Catégories',
            colorByPoint: true,
            data: [{
                name: '18-25',
                y: prop_age1825
            }, {
                name: '25-40',
                y: prop_age2540
            }, {
                name: '40-55',
                y: prop_age4055
            }, {
                name: '55+',
                y: prop_age55plus
            }]
        }],
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ['printChart', 'separator', 'downloadPDF', 'downloadPNG', 'downloadSVG', 'separator', 'downloadXLS']
                }
            }
        }
    });
}

function getHFGraphForAll() {
    var url = "../controllers/link_profiling.php?donnee=hfall";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawHFGraph(data[0], data[1]);
    }, dataType = "json");
}

function getHFGraphForFidele() {
    var url = "../controllers/link_profiling.php?donnee=hffidele";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawHFGraph(data[0], data[1]);
    }, dataType = "json");
}

function getHFGraphForGP() {
    var url = "../controllers/link_profiling.php?donnee=hfgp";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawHFGraph(data[0], data[1]);
    }, dataType = "json");
}

function drawHFGraph(prop_h, prop_f) {

    if(prop_h<=0){prop_h=null;}
    if(prop_f<=0){prop_f=null;}

    Highcharts.chart('container1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: 'Pourcentage<br>Homme / Femme<br>2018',
            align: 'center',
            verticalAlign: 'middle',
            y: 40
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%'],
                size: '110%'
            }
        },
        series: [{
            type: 'pie',
            name: 'Pourcentage H/F',
            innerSize: '50%',
            data: [{
                name: 'Homme',
                y: prop_h
            }, {
                name: 'Femme',
                y: prop_f
            }]
        }],
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ['printChart', 'separator', 'downloadPDF', 'downloadPNG', 'downloadSVG', 'separator', 'downloadXLS']
                }
            }
        }
    });
}

function getCSPGraphForAll() {
    var url = "../controllers/link_profiling.php?donnee=cspall";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawCSPGraph(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
    }, dataType = "json");
}

function getCSPGraphForFidele() {
    var url = "../controllers/link_profiling.php?donnee=cspfidele";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawCSPGraph(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
    }, dataType = "json");
}

function getCSPGraphForGP() {
    var url = "../controllers/link_profiling.php?donnee=cspgp";
    $.get(url, success = function (data) {
        //traitement de donnee met ici
        drawCSPGraph(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
    }, dataType = "json");
}

function drawCSPGraph(prop_1, prop_2, prop_3, prop_4, prop_5, prop_6, prop_7, prop_8) {

    if(prop_1<=0){prop_1=null;}
    if(prop_2<=0)prop_2=null;
    if(prop_3<=0)prop_3=null;
    if(prop_4<=0){prop_4=null;}
    if(prop_5<=0)prop_5=null;
    if(prop_6<=0)prop_6=null;
    if(prop_7<=0){prop_7=null;}
    if(prop_8<=0)prop_8=null;

    Highcharts.chart('container3', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Categories socioprofessionelles'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Pourcentage',
            colorByPoint: true,
            data: [{
                name: 'Agriculteurs exploitants',
                y: prop_1
            }, {
                name: 'Artisans, commerçants et chefs d’entreprise',
                y: prop_2
            }, {
                name: 'Cadres et professions intellectuelles supérieures',
                y: prop_3
            }, {
                name: 'Professions Intermédiaires',
                y: prop_4
            }, {
                name: 'Employés',
                y: prop_5
            }, {
                name: 'Ouvriers',
                y: prop_6
            }, {
                name: 'Retraités',
                y: prop_7
            }, {
                name: 'Autres personnes sans activités professionnelles',
                y: prop_8
            }]
        }],
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ['printChart', 'separator', 'downloadPDF', 'downloadPNG', 'downloadSVG', 'separator', 'downloadXLS']
                }
            }
        }
    });
}