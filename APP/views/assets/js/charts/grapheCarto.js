/*
**Créer un nouveau leaflet map online
*/

/*
var map = L.map('map');
L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}').addTo(map);
*/

/*
**Créer un nouveau leaflet map offline
*/
var osmUrl = '../views/assets/img/Tiles/{z}/{x}/{y}.png',
             osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
             osm = L.tileLayer(osmUrl, {
                    minZoom: 8,
                    maxZoom: 12,
                    attribution: osmAttrib
            });
var map = L.map('map').addLayer(osm);

var geojsonLayer;
var legend = L.control({ position: 'bottomleft' });

// Afficher des zone Iris avec differents couleurs en fonction du nombre de clients
function choixTypeClient(type) {
    switch (type) {
        case "total":
            mainCarto('total');
            break;
        case "fidele":
            mainCarto('fidele');
            break;
        case "grosP":
            mainCarto('grosP');
            break;
    }
}

// fonction affichant la carte et les contours iris
function mainCarto(typeClient) {
    $.get("../views/assets/js/data/contours-iris-2015.json", function (data, status) {
        var dataAPI = [];
        for (let i = 0; i < data.length; i++) {
            let code_iris = data[i].fields.code_iris;
            let nom_iris = data[i].fields.nom_iris;
            let contour_iris = data[i].fields.geo_shape["coordinates"];

            dataAPI.push({
                "code_iris": code_iris,
                "nom_iris": nom_iris,
                "contour_iris": contour_iris
            });
        };
                console.log(typeClient);
                console.log(dataAPI);
        BDD_link_carto(dataAPI, typeClient);
    });
}

/*Fonction permet de calculer le centre des zones Iris*/
function centreIrisClient(geojson){
    
}

/*Fonction permet de desiner des contour de IRIS avec differents couleurs
et ajouter legend
*/
function createCoutourIris(geojson, max, nbrIris) {
    // Ajouter des contours IRIS
    geojsonLayer = L.geoJson(geojson, {
        style: style,
        onEachFeature: onEachFeature
    }).addTo(map);

    // Legend

    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, Math.ceil(max * 0.25),  Math.ceil(max * 0.5), Math.ceil(max * 0.75), max];

        // loop des intrvalles de densité et générer une étiquette avec un carré de couleur pour chaque intervalle
        for (var i = 0; i < grades.length-1; i++) {
            if(i == grades.length-2){
                div.innerHTML += '<i style="background:' + getColor(grades[i]+1, max) + '"></i> ' + grades[i] + "-" + (grades[i+1]);
            }
            else{
                div.innerHTML += '<i style="background:' + getColor(grades[i]+1, max) + '"></i> ' + grades[i] + "-" + (grades[i+1]-1)+ '<br>';
            }
        }

        return div;
    };
    legend.addTo(map);      
}

/*Fonction permet de structurer des donnees(API/BDD)*/
function BDD_link_carto(dataAPI, typeClient) {
    var url = "../controllers/link_carto.php?type=" + typeClient;
    console.log(typeClient);
    $.ajax({
        type: "get",
        url: url,
        dataType: "json",
        success: function (dataBDD) {
            //console.log(dataBDD);
            //console.log(dataAPI);
            var nbrClient = 0;
            var geojson = {
                type: "FeatureCollection",
                features: []
            };
            //Organier structure de variable geojson
            dataBDD["json"].forEach(ele => {
                if (typeClient === 'total') { nbrClient = ele.nb_cli_total; }
                if (typeClient == 'fidele') { nbrClient = ele.nb_cli_fidele; }
                if (typeClient == 'grosP') { nbrClient = ele.nb_cli_gp; }
                
              //nbr_client<10, pas prendre en compte
                for (let i = 0; i < dataAPI.length; i++) {
                    if (ele["code_iris"] == dataAPI[i].code_iris) {
                        geojson.features.push({
                            "type": "Feature",
                            "properties": {
                                "code_iris": dataAPI[i].code_iris,
                                "nom_iris": dataAPI[i].nom_iris,
                                "nbr_client": nbrClient,
                                "max_client": dataBDD["max"]
                            },

                            "geometry": {
                                "type": "Polygon",
                                "coordinates": dataAPI[i].contour_iris
                            }
                        });
                    };
                };
            });
            //Focus de carte
            map.setView([dataBDD["latitude"], dataBDD["longitude"]], 10);
            var marker = L.marker([dataBDD["latitude"], dataBDD["longitude"]])
                        .addTo(map)
                        .bindPopup("<b>vous êtes ici</b>")
                        .openPopup();
            createCoutourIris(geojson,dataBDD["max"] );
        }
    });
}

/*Définir des coleurs en fonction des nombres de clients */
function getColor(d, max) {
    return  d > max * 0.75 ? '#E31A1C' :
                d > max * 0.5? '#FC4E2B' :
                    d > max * 0.25 ? '#FD8D3C' :
                        '#FFEDA0';
}

/*Styling fonction pour Layers de GeoJson*/
function style(feature) {
    return {
        fillColor: getColor(feature.properties["nbr_client"], feature.properties["max_client"]),
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

/*Function interation: mouseover*/
function highlightFeature(e) {
    var layer = e.target;
    layer.setStyle({
        weight: 5,
        color: '#54e3e8',
        dashArray: '',
        fillOpacity: 0.7
    });
    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }
}

/*Function interation: mouseout*/
function resetHighlight(e) {
    geojsonLayer.resetStyle(e.target);
}

/*Définir un listener click qui zoome à un contour IRIS*/
function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });

    layer.bindPopup('<h4>' + feature.properties.nom_iris + '</h4><h5>nombre de clients: ' + feature.properties.nbr_client + '</h5>');
}