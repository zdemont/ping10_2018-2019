// url du controleurs php chargé de préparer les données à envoyer aux graphes
var url = "../controllers/link_retailer_dashboard.php?func=";

/*Cette fonction a deux rôles:
    -récupérer les données en faisant appel au controleur
    -afficher le graphe
*/
function printGraphFrequentation() {
    $.get(url + "frequentation",
        success = function (data) {
            //console.log(data[0][1]);
            chartDataFreq(data);
        }, "json");
}
printGraphFrequentation();// appel de la fonction pour que le graphe soit affiché au chargement de la page

// Fonction qui dessine de graphe
function chartDataFreq(data) {
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        colors: ['#006745'],
        title: {
            text: 'Diagramme de la fréquentation dans votre établissement'
        },
        subtitle: {
            text: 'Valeurs prises sur la moyenne des fréquentations selon le jour'
        },
        xAxis: {
            type: 'Fréquentation de votre établissement',
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            title: {
                text: 'Nombre moyen de clients'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: false,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y: .f}</b> Personne(s)<br/>'
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ['printChart', 'separator', 'downloadPDF', 'downloadPNG', 'downloadSVG', 'separator', 'downloadXLS']
                }
            }
        },
        "series": [
            {
                "name": "Jour de la semaine",
                "colorByPoint": true,
                //data frequentation
                "data": [
                    {
                        "name": "Lundi",
                        "y": data[1][1],
                        "drilldown": "Lundi"
                    },
                    {
                        "name": "Mardi",
                        "y": data[2][1],
                        "drilldown": "Mardi"
                    },
                    {
                        "name": "Mercredi",
                        "y": data[3][1],
                        "drilldown": "Mercredi"
                    },
                    {
                        "name": "Jeudi",
                        "y": data[4][1],
                        "drilldown": "Jeudi"
                    },
                    {
                        "name": "Vendredi",
                        "y": data[5][1],
                        "drilldown": "Vendredi"
                    },
                    {
                        "name": "Samedi",
                        "y": data[6][1],
                        "drilldown": "Samedi"
                    },
                    {
                        "name": "Dimanche",
                        "y": data[0][1],
                        "drilldown": "dimanche"
                    }
                ]
            }
        ],
    });
};

