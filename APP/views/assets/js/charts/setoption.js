

Highcharts.setOptions({
    lang: {
        months: [
            'Janvier', 'Février', 'Mars', 'Avril',
            'Mai', 'Juin', 'Juillet', 'Août',
            'Septembre', 'Octobre', 'Novembre', 'Décembre'
        ],
        weekdays: [
            'Dimanche', 'Lundi', 'Mardi', 'Mercredi',
            'Jeudi', 'Vendredi', 'Samedi'
        ],
        downloadPDF: [
            'Téléchargement format PDF'
        ],
        downloadCSV: [
            'Téléchargement format CSV'
        ],
        downloadJPEG: [
            'Téléchargement format JPEG'
        ],
        downloadPDF: [
            'Téléchargement format PDF'
        ],
        downloadPNG: [
            'Téléchargement format PNG'
        ],
        downloadSVG: [
            'Téléchargement format SVG'
        ],
        downloadXLS: [
            'Téléchargement format XLS'
        ],
        printChart: [
            'Imprimer le graphique'
        ],
        viewData: [
            'Voir les données'
        ],
        loading: 'Chargement en cours...',
        printButtonTitle: 'Imprimer le graphique',
        resetZoom: 'Réinitialiser le zoom',
        resetZoomTitle: 'Réinitialiser le zoom au niveau 1:1',
        thousandsSep: ' ',
        decimalPoint: ','
    }
});