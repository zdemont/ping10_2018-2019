function changeName() {
    var CheminComplet = document.location.href;
    var CheminRepertoire  = CheminComplet.substring( 0 ,CheminComplet.lastIndexOf( "/" ) );
    var name = CheminComplet.substring(CheminComplet.lastIndexOf( "/" )+1 );

    switch(name) {
        case "retailer_dashboard.phtml":
            document.getElementById("pagename").innerHTML = "Votre activité";
        break;
        case "retailer_cartographie.phtml":
            document.getElementById("pagename").innerHTML = "Localiser mes clients";
        break;  
        case "retailer_profiling.phtml":
            document.getElementById("pagename").innerHTML = "Connaître mes clients";
        break;  
        case "retailer_web_marketing.phtml":
            document.getElementById("pagename").innerHTML = "Ma présence sur le web";
        break;
        case "retailer_start.phtml":
            document.getElementById("pagename").innerHTML = "Accueil";
        break;    
    }
};