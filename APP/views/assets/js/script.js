let bot = new RiveScript();

const brains = [
   'assets/rive/brain.rive'
];
bot.loadFile(brains).then(botReady).catch(botNotReady);
const sleep = (milliseconds) => {
   return new Promise(resolve => setTimeout(resolve, milliseconds))
 }
 


const message_container = document.querySelector('.messages');
const form = document.querySelector('form');
const input_box = document.querySelector('input');
form.addEventListener("submit", (e) => {
 e.preventDefault();
 selfReply(input_box.value);
 input_box.value = "";
});
function botReply(message){
   sleep(700).then(() => {
      message_container.innerHTML += `<div class="bot">${message}</div>`;
      location.href = "#edge";
      $("#chatbox").focus();
    })
 
}
function selfReply(message){
 message_container.innerHTML += `<div class="self">${message}</div>`;
 location.href = "#edge";
 
 bot.reply("local-user", message).then(function(reply) {
 botReply(reply);
 });
}
function botReady(){
 bot.sortReplies();
 botReply("Bonjour, je suis votre assistant Crédit Agricole !");
 botReply("Je peux vous aider à utiliser votre outil ! Besoin d'aide ? Tapez 'exemple' pour obtenir une suggestion de requête !");
 $("#chatbox").focus();
}
function botNotReady(err){
 console.log("An error has occurred.", err);
}