<footer class="fixed-bottom">
            <div class="chat" id ="myForm">
            <script>document.getElementById("myForm").style.display = "none";</script>
              <div class="messages"></div>
              <div id="edge"></div>
              <form class="actions">
                <input type="text" placeholder="Appuyez sur 'Entrée' pour parler" id="chatbox" autofocus="autofocus" autocomplete="off">
              </form> 
              </div>
              <button type="button" class="open-button" onclick="openForm()">Mon assistant Crédit Agricole</button>
        
              <script src="assets/rive/rivescript.min.js"></script>
              <script src="assets/js/script.js"></script>
              <script>
                var isBotOpen;
        
                function openForm() {
                    if (window.isBotOpen === true)
                    {
                      document.getElementById("myForm").style.display = "none";
                      isBotOpen = false;
                    }
                    else
                    {
                      document.getElementById("myForm").style.display = "block";
                      document.getElementById("chatbox").focus();
                      isBotOpen = true; 
                    }
                }
              
              </script>
          </footer>