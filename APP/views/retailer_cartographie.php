<?php
require('../controllers/verif_authentification.php');
?>
<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Carte</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="assets/vendors/icheck/skins/all.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="assets/images/favicon.png"/>

    <!--hightcharts scripts-->
    <script src="assets/highcharts/highcharts.js"></script>
    <script src="assets/highcharts/exporting.js"></script>
    <script src="assets/highcharts/export-data.js"></script>
    <script src="assets/highcharts/drilldown.js"></script>
    <script> src = "assets/js/charts/frequentation.js"</script>
    <script> src = "assets/js/charts/chiffreAffaire.js"</script>
    <script> src = "assets/js/charts/setoption.js"</script>
    <link rel="stylesheet" href="assets/css/bot.css">
    <script src="assets/jquery/jquery.min.js"></script>
    <style>

        #map {
            width: auto;
            height: 750px;
        }

        .legend {
            line-height: 45px;
            color: #555;
            padding: 6px 8px;
            font: 18px Helvetica, sans-serif;
            background: white;
            background: rgba(255, 255, 255, 0.8);
            box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
            border-radius: 5px;
        }

        .legend i {
            width: 20px;
            height: 18px;
            float: left;
            margin-right: 4px;
            opacity: 0.7;
        }
    </style>

    <!-- Resources -->
    <link rel="stylesheet" href="assets/leaflet/leaflet.css"/>
    <script src="assets/leaflet/leaflet.js"></script>
</head>

<body>
<!-- Intégration du header -->
<?php
if (isset($_SESSION['userAdviser'])) {
    include "header_conseiller.php";
} elseif ($_SESSION['userRetailer']) {
    include "header_retailer.php";
}
?>
<!-- partial -->
<div class="container-fluid page-body-wrapper">
    <!-- Intégration sidebar -->
    <?php include "sidebar.php" ?>
    <div class="main-panel">
        <div class="d-flex justify-content-center grid-margin">
            <button class="btn btn-primary btn-fw" onclick="choixTypeClient('total')" type="button"
                    style="margin-left: 10px; margin-right: 10px; margin-top: 15px"> Client Total
            </button>
            <button class="btn btn-primary btn-fw" onclick="choixTypeClient('fidele')" type="button"
                    style="margin-left: 10px; margin-right: 10px; margin-top: 15px"> Client Fidèle
            </button>
            <button class="btn btn-primary btn-fw" onclick="choixTypeClient('grosP')" type="button"
                    style="margin-left: 10px; margin-right: 10px; margin-top: 15px"> Client Gros Panier
            </button>
        </div>
        <div class="content-wrapper">
            <div id="map" class="map"></div>
        </div>
    </div>
</div>

<?php include "footer_standard.php" ?>

<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<!--<script src="assets/js/Popup-Element-Overlay.js"></script>
<script src="assets/js/Subscribe-Popup-Window.js"></script>-->

<!--Script pour carto-->
<script src="../views/assets/js/charts/grapheCarto.js"></script>
<script>
    mainCarto("total");
</script>
</body>

</html>