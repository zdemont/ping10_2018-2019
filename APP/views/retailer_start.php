<?php
require ('../controllers/verif_authentification.php');
include_once ('../controllers/retailer_accueil.php');

?>

<html lang="en">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Accueil</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="assets/vendors/icheck/skins/all.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />
    <link rel="stylesheet" href="assets/css/Card-Group-Classic.css">

    <!--hightcharts scripts-->
    <script src="assets/highcharts/highcharts.js"></script>
    <script src="assets/highcharts/exporting.js"></script>
    <script src="assets/highcharts/export-data.js"></script>
    <script src="assets/highcharts/drilldown.js"></script>
    <script> src="assets/js/charts/frequentation.js"</script>
    <script> src="assets/js/charts/chiffreAffaire.js"</script>
    <script> src="assets/js/charts/setoption.js"</script>
    <link rel="stylesheet" href="assets/css/bot.css">
    <script src="assets/jquery/jquery.min.js"></script>

    <link rel="stylesheet" href="assets/fonts/all.css">

  </head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php 
    if (isset($_SESSION['userAdviser'])){
        include "header_conseiller.php";
    }
    elseif ($_SESSION['userRetailer']){
        include "header_retailer.php";
    }
    ?>
  </div>
    <!-- partial -->
  <div class="container" style="margin-top : 50px">
    <div class="row">
          <div class="col-md-4 col-xl-5 offset-md-1 offset-lg-1 offset-xl-0 cust_blogteaser">
            <a href="retailer_dashboard.php"><img class="img-fluid" src="assets/img/reporting.jpg"></a>
            <h3>Analyser mon activité</h3>
            <a href="retailer_dashboard.php" class="h4"><i class="fa fa-arrow-circle-right"></i></a>
          </div>
          <div class="col-md-4 col-xl-5 offset-md-2 offset-lg-2 offset-xl-2 cust_blogteaser">
            <a href="retailer_cartographie.php"><img class="img-fluid" src="assets/img/cartography.jpg"></a>
            <h3>Localiser mes clients</h3>
            <a href="retailer_cartographie.php" class="h4"><i class="fa fa-arrow-circle-right"></i></a>
          </div>
          <div class="col-md-4 col-xl-5 offset-md-1 offset-lg-1 offset-xl-0 cust_blogteaser">
            <a href="retailer_profiling.php"><img class="img-fluid" src="assets/img/profiling.jpg"></a>
            <h3>Connaître mes clients</h3>
            <a href="retailer_profiling.php" class="h4"><i class="fa fa-arrow-circle-right"></i></a>
          </div>
          <div class="col-md-4 col-xl-5 offset-md-2 offset-lg-2 offset-xl-2 cust_blogteaser">
            <a href="retailer_web_marketing.php"><img class="img-fluid" src="assets/img/web-marketing.jpg"></a>
            <h3>Ma présence sur le web</h3>
            <a href="retailer_web_marketing.php" class="h4"><i class="fa fa-arrow-circle-right"></i></a></div>
          </div>
    </div>
  </div>
  <?php include "footer_start.php"?>
  <br>
  <br>
  <br>
  <br>
    <!-- container-scroller -->
</body>
  <!-- plugins:js -->
  <script src="assets/vendors/js/vendor.bundle.base.js"></script>
  <script src="assets/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="assets/js/off-canvas.js"></script>
  <script src="assets/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="assets/js/dashboard.js"></script>
  <!-- End custom js for this page-->