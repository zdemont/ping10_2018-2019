<footer class="fixed-bottom">
    <div class="chat" id ="myForm">
      <div class="messages"></div>
      <div id="edge"></div>
      <form class="actions">
        <input class="inputchat" type="text" placeholder="Appuyez sur 'Entrée' pour parler" id="chatbox" autofocus="autofocus" autocomplete="off">
      </form> 
      </div>
      <button type="button" class="open-button" onclick="openForm()">Mon assistant Crédit Agricole</button>

      <script src="assets/rive/rivescript.min.js"></script>
      <script src="assets/js/script.js"></script>
      <script type="text/javascript">
        document.getElementById("chatbox").focus();
      </script>
      <script>
        var isBotOpen;

        function openForm() {
          if (window.isBotOpen === true || window.isBotOpen == undefined)
          {
            document.getElementById("myForm").style.display = "none";
            isBotOpen = false;
          }
          else
          {
            document.getElementById("myForm").style.display = "block";
            isBotOpen = true; 
          }
          $("#chatbox").focus();
        }
      
      </script>
  </footer>