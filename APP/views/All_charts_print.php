<div class="row">
    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="cont12" class="h-100">

                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="cont15" class="h-100">

                </div>
            </div>
        </div>
    </div>

</div>

<div class="d-flex justify-content-center grid-margin">
    <h2>Profiling de mes clients fidèles </h2>
</div>

<!--graphique Age client fidèles -->
<div class="row">

    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="cont14" class="h-100">
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="cont6" class="h-100">

                </div>
            </div>
        </div>
    </div>
</div>


<!-- graphique Homme Femme pour clients fidèles -->
<div class="row">
    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="cont7" class="h-100">

                </div>
            </div>
        </div>
    </div>

    <!-- ajouter le nouveau grapique ici  -->
    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="cont8" class="h-100">
                </div>
            </div>
        </div>
    </div>

</div>

<div class="d-flex justify-content-center grid-margin">
    <h2>Profiling de mes clients Gros Panier </h2>
</div>
<!-- graphique Age Client Gros Panier -->

<div class="row">
    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="cont9" class="h-100">
                </div>
            </div>
        </div>
    </div>


    <!-- graphique categories socio-professionelles pour les clients Gros Panier -->


    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="cont5" class="h-100">
                </div>
            </div>
        </div>
    </div>
</div>


<!-- graphique Homme Femme pour client GP -->

<div class="row">
    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="cont10" class="h-100">

                </div>
            </div>
        </div>
    </div>


    <!-- graphique foyer clients GP  -->
    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div id="cont11" class="h-100">

                </div>
            </div>
        </div>
    </div>

</div>


</div>

</div>
<script src="assets/vendors/js/vendor.bundle.base.js"></script>
<script src="assets/vendors/js/vendor.bundle.addons.js"></script>


<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="assets/js/off-canvas.js"></script>
<script src="assets/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="assets/js/dashboard_kpi.js"></script>
<script>
    //chiffre d'affaire du mois précédent
    getLastCA();
    //Evolution du CA
    getEvolCA();
    getFoyerGraphForGP();

    getHFGraphForGP();
    getCSPGraphForGP();
    getAgeGraphForGP();
    getFoyerGraphForFidele();
    getHFGraphForFidele();
    getCSPGraphForFidele();
    getAgeGraphForFidele();
    getFoyerGraphForAll();
    getHFGraphForAll();
    getCSPGraphForAll();
    getAgeGraphForAll();
    printGraphFrequentation();
</script>
