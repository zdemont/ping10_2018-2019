<?php
require('../controllers/verif_authentification.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Connaître mes clients</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="assets/vendors/icheck/skins/all.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="assets/img/favicon.png"/>

    <!--hightcharts scripts-->
    <script src="assets/highcharts/highcharts.js"></script>
    <script src="assets/highcharts/exporting.js"></script>
    <script src="assets/highcharts/export-data.js"></script>
    <script src="assets/highcharts/drilldown.js"></script>
    <script> src = "assets/js/charts/setoption.js"</script>
    <link rel="stylesheet" href="assets/css/bot.css">

</head>

<body>
<!-- Intégration du header -->
<?php
if (isset($_SESSION['userAdviser'])) {
    include "header_conseiller.php";
} elseif ($_SESSION['userRetailer']) {
    include "header_retailer.php";
}
?>
<!-- partial -->
<div class="container-fluid page-body-wrapper">
    <!-- Intégration sidebar -->
    <?php include "sidebar.php" ?>
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="d-flex justify-content-center grid-margin">
                <button class="btn btn-primary btn-fw" type="button" onclick="profilingTotal()"
                        style="margin-left: 10px; margin-right: 10px;"> Total
                </button>
                <button class="btn btn-primary btn-fw" type="button" onclick="profilingFidele()"
                        style="margin-left: 10px; margin-right: 10px;"> Fidèles
                </button>
                <button class="btn btn-primary btn-fw" type="button" onclick="profilingGrosPanier()"
                        style="margin-left: 10px; margin-right: 10px;"> Gros panier
                </button>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 grid-margin stretch-card">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="clearfix">
                                <div class="float-left">
                                    <i class="mdi mdi-shopping text-danger icon-lg"></i>
                                </div>
                                <div class="float-right">
                                    <p class="mb-0 text-right">Panier Moyen</p>
                                    <div class="fluid-container">
                                        <h3 class="font-weight-medium text-right mb-0" id="paniermoy"></h3>
                                    </div>
                                </div>
                            </div>
                            <p class="text-muted mt-3 mb-0">
                                <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Sur les 3 derniers mois
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 grid-margin stretch-card">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="clearfix">
                                <div class="float-left">
                                    <i class="mdi mdi-account-star text-warning icon-lg"></i>
                                </div>
                                <div class="float-right">
                                    <p class="mb-0 text-right">Clients fidèles</p>
                                    <div class="fluid-container">
                                        <h3 class="font-weight-medium text-right mb-0" id="cli_fid_total"></h3>
                                    </div>
                                </div>
                            </div>
                            <p class="text-muted mt-3 mb-0">
                                <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Sur l'année 2018
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 grid-margin stretch-card">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="clearfix">
                                <div class="float-left">
                                    <i class="mdi mdi-briefcase text-success icon-lg"></i>
                                </div>
                                <div class="float-right">
                                    <p class="mb-0 text-right">Clients pro</p>
                                    <div class="fluid-container">
                                        <h3 class="font-weight-medium text-right mb-0" id="prop_cli_pro"></h3>
                                    </div>
                                </div>
                            </div>
                            <p class="text-muted mt-3 mb-0">
                                <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Sur l'année 2018
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container4" class="h-100"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container1" class="h-100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container2" class="h-100"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div id="container3" class="h-100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <br><br><br>
        </div>
        <!-- content-wrapper ends -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<?php include "footer_standard.php" ?>
</body>
<!-- plugins:js -->
<script src="assets/vendors/js/vendor.bundle.base.js"></script>
<script src="assets/vendors/js/vendor.bundle.addons.js"></script>

<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="assets/js/off-canvas.js"></script>
<script src="assets/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<!-- End custom js for this page-->

<!-- Graph scripts importation -->
<script src="assets/js/charts/grapheProfiling.js"></script>
<script>
    //Appel des fonctions du script graphProfiling.js
    profilingTotal();
    getPanierMoy();
    getCliFidele();
    getClipro();
</script>
<script src="assets/js/charts/setoption.js"></script>