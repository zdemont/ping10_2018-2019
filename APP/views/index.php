<?php
    session_start();
    if (!isset($_SESSION['error'])){
        $_SESSION['error']=0;
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Booster By CA</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/css/modal/Google-Style-Login.css">
    <link rel="stylesheet" href="assets/css/Pretty-Footer.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="assets/images/favicon.png"/>
</head>

<body>
<nav class="navbar navbar-light navbar-expand sticky-top bg-white">
    <div class="container-fluid">
        <a class="navbar-brand text-success" href="index.php">Booster by CA</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse mr-auto" id="navcol-1">
            <ul class="nav navbar-nav"></ul>
            <a class="btn btn-primary ml-auto" role="button" href="#myModal" data-toggle="modal"
               style="padding:6px 12px;margin-bottom:0px;background-color:#006a4e;">Se connecter</a>
        </div>
    </div>
</nav>
<div class="modal fade" role="dialog" tabindex="-1" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="login-card profile-img-card" style="margin-top:25px;"><img
                            src="assets/img/faces/avatar_2x.png" class="profile-img-card">
                    <p class="profile-name-card"></p>
                    <form method="post" action="../controllers/connexion.php" class="form-group">
                        <div class=" form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="id">Identifiant :</label>
                            <input type="text" class="form-control" id="id" name="id" required/>
                        </div>

                        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="password">Mot de passe :</label>
                            <input type="password" class="form-control" id="password" name="password" required/>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <button class="btn btn-primary btn-block btn-lg btn-signin" name="connexion" type="submit"
                                    style="margin-left:0px;">Se connecter
                            </button>
                        </div>
                    </form>
                    <a href="#" class="forgot-password">Vous avez oublié vos identifiants ?</a>
                    <!-- Message d'erreur en cas de problème de connexion -->
                    <div class="alert alert-warning alert-dismissible fade show" role="alert" id="conn-err-msg" style="display:<?php if($_SESSION['error']){echo "on";}else{echo "none";} ?>" >
                        <strong>Erreur de connexion!</strong> Mauvais couple Identifiant/Mot de passe.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<main class="page landing-page">
    <section class="clean-block clean-hero"
             style="background-image:url(&quot;assets/img/Shops.jpeg&quot;);color:rgba(0,45,72,0.85);">
        <div class="text">
            <h2>Complet, simple, flexible</h2>
            <p>Un outil de gestion de votre commerce complet de A à Z</p><a class="btn btn-outline-light btn-lg"
                                                                            role="button" href="#modal_help"
                                                                            data-toggle="modal">En savoir plus</a></div>
    </section>
    <div class="modal fade" role="dialog" tabindex="-1" id="modal_help" data-toggle="modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Bienvenue dans Booster By CA</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Ce service met en relation les données issues de
                        votre Terminal de Paiement Electronique et celles du
                        réseau de professionnel Crédit Agricole pour
                        vous donner les meilleurs outils d'analyse
                        de votre activité. Analysez vos performances
                         et rapprochez vous toujours davantage
                        de votre clientèle<br><br>
                        
                        <br><br>

                        Bon pilotage !
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-light" type="button" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <section class="clean-block features">
        <div class="container">
            <div class="block-heading">
                <h2 class="text-info">Fonctionalités</h2>
                <p>Principales fonctionalités du site</p>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-5 feature-box"><i class="icon-star icon"></i>
                    <h4>Analyser votre activité</h4>
                    <p>Graphiques de votre activité pour mieux piloter votre commerce</p>
                </div>
                <div class="col-md-5 feature-box"><i class="icon-pencil icon"></i>
                    <h4>Localisation</h4>
                    <p>Prévoyez vos campagnes marketing avec une visualisation de vos zones clientelles</p>
                </div>
                <div class="col-md-5 feature-box"><i class="icon-screen-smartphone icon"></i>
                    <h4>Connaître mes clients</h4>
                    <p>Adaptez votre commerce aux moeurs de vos clients</p>
                </div>
                <div class="col-md-5 feature-box"><i class="icon-refresh icon"></i>
                    <h4>Présence sur internet</h4>
                    <p>Evaluer votre présence sur le web</p>
                </div>
            </div>
        </div>
    </section>
</main>

<div class="footer-clean" style="padding-top:0px;">
    <footer>
        <div class="row">
            <div class="col-sm-6 col-md-4 footer-navigation">
                <h3></h3>
                <h2 style="color:#006a4e;padding-bottom:30px;">Credit Agricole</h2>
                <p class="links"><a href="#">Accueil</a><strong> · </strong><a
                            href="#">Blog</a><strong>&nbsp;· </strong><a href="#">A Propos</a><strong> · </strong><a
                            href="#">FAQ</a><strong> · </strong><a href="#">Contact</a></p>
                <p class="company-name">Crédit Agricole Normandie © 2018</p>
            </div>
            <div class="col-sm-6 col-md-4 footer-contacts">
                <div><span class="fa fa-map-marker footer-contacts-icon"> </span>
                    <p><span class="new-line-span">Cité de l'agriculture, 76230<br></span> Bois-Guillaume, France</p>
                </div>
                <div><i class="fa fa-phone footer-contacts-icon"></i>
                    <p class="footer-center-info email text-left"><a href="tel:+33231552424">02 31 55 24 24</a><br></p>
                </div>
                <div><i class="fa fa-envelope footer-contacts-icon"></i>
                    <p><a href="#" target="_blank">support@creditagricole.com</a></p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4 footer-about">
                <h4>A propos de l'entreprise</h4>
                <p> Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor
                    lacus vehicula sit amet. </p>
                <div class="social-links social-icons"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i
                                class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-linkedin"></i></a><a href="#"><i
                                class="fa fa-github"></i></a></div>
            </div>
        </div>
    </footer>
</div>

<script src="assets/vendors/js/vendor.bundle.base.js"></script>
<script src="assets/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="assets/js/off-canvas.js"></script>
<script src="assets/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->

</body>

</html>

<?php
    if(isset($_SESSION['error'])){ 
        echo '<script> $("#myModal").modal("show"); </script>';
        $_SESSION['error']=0;
    }
?>