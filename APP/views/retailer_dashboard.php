<?php
require ('../controllers/verif_authentification.php');
?>
<!DOCTYPE html>
<html lang="en">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Votre activité</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="assets/vendors/icheck/skins/all.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />

    <!--hightcharts scripts-->
    <script src="assets/highcharts/highcharts.js"></script>
    <script src="assets/highcharts/exporting.js"></script>
    <script src="assets/highcharts/export-data.js"></script>
    <script src="assets/highcharts/drilldown.js"></script>
    <script> src="assets/js/charts/frequentation.js"</script>
    <script> src="assets/js/charts/chiffreAffaire.js"</script>
    <script> src="assets/js/charts/setoption.js"</script>
    <link rel="stylesheet" href="assets/css/bot.css">
    <script src="assets/jquery/jquery.min.js"></script>
    


  </head>

<body>
    <!-- Intégration du header -->
    <?php
    if (isset($_SESSION['userAdviser'])){
        include "header_conseiller.php";
    }
    elseif ($_SESSION['userRetailer']){
        include "header_retailer.php";
    }
    ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- Intégration sidebar -->
      <?php include "sidebar.php"?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="d-flex justify-content-center grid-margin">
            <h2>Mes chiffres clés</h2>
          </div>
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin">
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-currency-eur text-danger icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Chiffre d'affaires du mois précédent en €</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" id="LastCA"> </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-trending-up text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Evolution du CA</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" id="EvolCA"> </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin">
            </div>
          </div>
          <div class="d-flex justify-content-center grid-margin">
              <h2>Données de fréquentation</h2>
          </div>
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div id="container" class="d-flex"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="d-flex justify-content-center grid-margin">
          
              <h2>Chiffre d'affaires par mois et par semaines en € </h2>
          </div>
          <div class="d-flex justify-content-center grid-margin">
            <button class= "btn btn-primary btn-fw" onclick= "printGraphCAMens()" type="button" style="margin-left: 10px; margin-right: 10px"> Mois</button>
            <button class= "btn btn-primary btn-fw" onclick= "printGraphCAHebdo()" type="button" style="margin-left: 10px; margin-right: 10px"> Semaines </button>
          </div>
          <div class="row grid-margin stretch-card">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div id="container1" class="h-100"></div>
                </div>
              </div>
            </div>
          </div>
        <!-- content-wrapper ends -->
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <?php include "footer_standard.php"?>

  <!-- plugins:js -->
  <script src="assets/vendors/js/vendor.bundle.base.js"></script>
  <script src="assets/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="assets/js/off-canvas.js"></script>
  <script src="assets/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="assets/js/dashboard_kpi.js"></script>
  <script>
      getLastCA();
      getEvolCA()
  </script>
  <script src="assets/js/charts/setoption.js"></script>
  <!-- End custom js for this page-->

  <!-- Importation des scripts des graphes -->
  <script src="assets/js/charts/grapheCA.js"></script>
  <script src="assets/js/charts/grapheFrequentation.js"></script>
  

</body>

</html>