<?php
require('../controllers/verif_authentification.php');
?>

<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Mon activité </title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="assets/vendors/icheck/skins/all.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="assets/img/favicon.png"/>

    <!--hightcharts scripts-->
    <script src="assets/highcharts/highcharts.js"></script>
    <script src="assets/highcharts/exporting.js"></script>
    <script src="assets/highcharts/export-data.js"></script>
    <script src="assets/highcharts/drilldown.js"></script>
    <script> src = "assets/js/charts/frequentation.js"</script>
    <script> src = "assets/js/charts/chiffreAffaire.js"</script>
    <script> src = "assets/js/charts/setoption.js"</script>


</head>

<body>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
            <h3 class="d-flex align-items-center">Booster By CA</h3>
            <a class="navbar-brand brand-logo-mini" href="#">
                <img src="assets/img/CA.svg" alt="logo"/>
            </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center bg-success">
            <h3>Page administration</h3>
            <ul class="navbar-nav navbar-nav-right">
                <li class="nav-item dropdown d-none d-xl-inline-block">
                    <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown"
                       aria-expanded="false">
                        <span class="profile-text">Administrateur</span>
                        <img class="img-xs rounded-circle" src="assets/img/faces/face1.png" alt="Profile image">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                        <a class="dropdown-item" role="button" href="../controllers/deconnexion.php">
                            Se déconnecter
                        </a>
                    </div>
                </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                <span class="mdi mdi-menu"></span>
            </button>
        </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
                <li>
                    <h4 style="text-align: center">Menu de navigation</h4>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin_homepage.php">
                        <i class="menu-icon mdi mdi-home"></i>
                        <span class="menu-title">Menu</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin_create_adviser.php">
                        <i class="menu-icon mdi mdi-chart-bar"></i>
                        <span class="menu-title">Ajouter un conseiller</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
            <form method="post" action="../controllers/admin_create_adviser_user.php" class="form-group">
                <h1>Créer de nouveaux conseillers</h1>
                <br><br>
                <?php
                if (isset($_SESSION['error_creation_adviser'])) {
                    foreach ($_SESSION['error_creation_adviser'] as $erreur) {
                        echo '<p style="color: red">' . $erreur . '</p>';
                    }
                }
                //var_dump($_SESSION);
                ?>

                <div class="form-row form-group">
                    <div class="col-sm-4 label-column"><label class="col-form-label" for="name-input-field">Nom</label>
                    </div>
                    <div class="col-sm-6 input-column"><input id="nom" name='nom' class="form-control" type="text"
                                                              required></div>
                </div>
                <div class="form-row form-group">
                    <div class="col-sm-4 label-column"><label class="col-form-label"
                                                              for="name-input-field">Prénom</label></div>
                    <div class="col-sm-6 input-column"><input id="prenom" name="prenom" class="form-control" type="text"
                                                              required></div>
                </div>
                <div class="form-row form-group">
                    <div class="col-sm-4 label-column"><label class="col-form-label" for="pawssword-input-field">Mot de
                            passe </label></div>
                    <div class="col-sm-6 input-column"><input id="password" name="password" class="form-control"
                                                              type="password" required></div>
                </div>
                <div class="form-row form-group">
                    <div class="col-sm-4 label-column"><label class="col-form-label" for="repeat-pawssword-input-field">Confirmer
                            mot de passe </label></div>
                    <div class="col-sm-6 input-column"><input id="passwordverif" name="passwordverif"
                                                              class="form-control" type="password" required></div>
                </div>
                <button class="btn btn-light submit-button" type="Submit" name="createAdviser">Créer nouvel
                    utilisateur
                </button>
            </form>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
        </footer>
        <!-- partial -->
    </div>
    <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
</body>
<!-- plugins:js -->
<script src="assets/vendors/js/vendor.bundle.base.js"></script>
<script src="assets/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="assets/js/off-canvas.js"></script>
<script src="assets/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="assets/js/dashboard.js"></script>
<!-- End custom js for this page-->

