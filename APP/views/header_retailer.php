<?php
include_once ("../models/dto/dto_commercant.php");

if (!isset($_SESSION)){
    session_start();
}

if(isset($_SESSION['userRetailer'])){
    $userRetailer = $_SESSION['userRetailer'];
    $retailerName =  $userRetailer->getPrenom()." ".$userRetailer->getNom();
}
?>

<div class="container-scroller">
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
            <h3 class="d-flex align-items-center">Booster By CA</h3>
            <a class="navbar-brand brand-logo-mini" href="../start.html">
            <img src="assets/img/CA.svg" alt="logo" />
            </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center bg-success">
            <h3 id="pagename"> </h3>
            <ul class="navbar-nav navbar-nav-right">
                <h4><?php echo $retailerName;?></h4>
                <li class="nav-item dropdown d-none d-xl-inline-block">
                <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                    <img class="img-xs rounded-circle" src="assets/img/faces/face1.png" alt="Profile image">
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                    <a class="dropdown-item" role="button" href="../controllers/deconnexion.php">
                    Se déconnecter
                    </a>
                </div>
                </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                <span class="mdi mdi-menu"></span>
            </button>
        </div>
    </nav>
</div>

<script src="assets/js/namepage.js"></script>
<script>changeName();</script>