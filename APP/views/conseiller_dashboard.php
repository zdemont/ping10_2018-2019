<?php
require('../controllers/verif_authentification.php');
include_once('../controllers/conseiller_dashboard.php');
?>

<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Accueil conseiller</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="assets/vendors/icheck/skins/all.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/table-style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="assets/img/favicon.png"/>

    <!--hightcharts scripts-->
    <script src="assets/highcharts/highcharts.js"></script>
    <script src="assets/highcharts/exporting.js"></script>
    <script src="assets/highcharts/export-data.js"></script>
    <script src="assets/highcharts/drilldown.js"></script>
    <script> src = "assets/js/charts/frequentation.js"</script>
    <script> src = "assets/js/charts/chiffreAffaire.js"</script>
    <script> src = "assets/js/charts/setoption.js"</script>


</head>

<body>
<div class="container-scroller">
    <!-- Intégration du header -->
    <div class="container-scroller">
        <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
                <h3 class="d-flex align-items-center">Booster By CA</h3>
                <a class="navbar-brand brand-logo-mini" href="#">
                    <img src="assets/img/CA.svg" alt="logo"/>
                </a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center bg-dark">
                <h3 id="pagename"></h3>
                <ul class="navbar-nav navbar-nav-right">
                    <h4> <?php echo $userAdviser->getPrenom() . " " . $userAdviser->getNom(); ?></h4>

                    <li class="nav-item dropdown d-none d-xl-inline-block">
                        <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown"
                           aria-expanded="false">
                            <img class="img-xs rounded-circle" src="assets/img/faces/face1.png" alt="Profile image">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                            <a class="dropdown-item" role="button" href="conseiller_dashboard.php">
                                Tableau clients CA
                            </a>
                            <a class="dropdown-item" role="button" href="../controllers/deconnexion.php">
                                Se déconnecter
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                        data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                </button>
            </div>
        </nav>
    </div>

    <script src="assets/js/namepage.js"></script>
    <script>changeName();</script>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
        </nav>
        <!-- partial -->

        <div class="main-panel">
            <div class="content-wrapper">
                <?php

                ?>
                <h3 class="grid-margin">Votre portefeuille de commerçants</h3>


                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <input type="text" id="myInput" onkeyup="myFunction()"
                                       placeholder="Rechercher par siret">
                                <table class="table table-hover" id="myTable">
                                    <thead>
                                    <tr>
                                        <th>Commercants</th>
                                        <th>Numéro de siret</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($userAdviser->getListCommercants() as $i) { ?>
                                        <tr>
                                            <td>
                                                <a href="../controllers/conseiller_show_retailer.php?id=<?php echo($i->getUserId()); ?>">
                                                    <?php echo(getCommercantName($i)); ?>
                                                </a>
                                            </td>

                                            <td>
                                                <a href="../controllers/conseiller_show_retailer.php?id=<?php echo($i->getUserId()); ?>">
                                                    <?php echo($i->getNumSiret()); ?>
                                                </a>
                                            </td>


                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <footer class="footer">
                </footer>
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <script src="assets/js/advirser_retailer_search.js"></script>
    <!-- plugins:js -->
    <script src="assets/vendors/js/vendor.bundle.base.js"></script>
    <script src="assets/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="assets/js/off-canvas.js"></script>
    <script src="assets/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="assets/js/dashboard.js"></script>
    <!-- End custom js for this page-->