<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 15/01/2019
 * Time: 14:12
 */

include_once ("dao/dao_ca_hebdo.php");
include_once ("dao/dao_ca_mensuelle.php");
include_once ("dao/dao_carto_client.php");
include_once ("dao/dao_frequentation.php");
include_once ("dao/dao_reporting_client.php");
include_once ("dao/dao_reporting_client_fidele.php");
include_once ("dao/dao_reporting_client_gp.php");

/**
 * Methode permettant d'instancier la DAO et excute la methode getCaHebdoDB
 *
 * @param $numSiret
 * @return array|int
 */
function getCaHebdo ($numSiret){
    $objet = new DAOCaHebdo();
    return $objet->getCaHebdoDB($numSiret);
}

/**
 * Methode permettant d'instancier la DAO et excute la methode getCaMensuelleDB
 *
 * @param $numSiret
 * @return array|int
 */
function getCaMensuelle ($numSiret){
    $objet = new DAOCaMensuelle();
    return $objet->getCaMensuelleDB($numSiret);
}

/**
 * Methode permettant d'instancier la DAO et excute la methode getCartoClientDB
 *
 * @param $numSiret
 * @return array|int
 */
function getCartoClient ($numSiret){
    $objet = new DAOCartoClient();
    return $objet->getCartoClientDB($numSiret);
}

/**
 * Methode permettant d'instancier la DAO et excute la methode getFrequentationDB
 *
 * @param $numSiret
 * @return array|int
 */
function getFrequentation ($numSiret){
    $objet = new DAOFrequentation();
    return $objet->getFrequentationDB($numSiret);
}

/**
 * Methode permettant d'instancier la DAO et excute la methode getReportingClientDB
 *
 * @param $numSiret
 * @return array|int
 */
function getReportingClient ($numSiret){
    $objet = new DAOReportingClient();
    return $objet->getReportingClientDB($numSiret);
}

/**
 * Methode permettant d'instancier la DAO et excute la methode getReportingClientFideleDB
 *
 * @param $numSiret
 * @return array|int
 */
function getReportingClientFidele ($numSiret){
    $objet = new DAOReportingClientFidele();
    return $objet->getReportingClientFideleDB($numSiret);
}

/**
 * Methode permettant d'instancier la DAO et excute la methode getReportingClientGPDB
 *
 * @param $numSiret
 * @return array|int
 */
function getReportingClientGP ($numSiret){
    $objet = new DAOReportingClientGP();
    return $objet->getReportingClientGPDB($numSiret);
}
?>