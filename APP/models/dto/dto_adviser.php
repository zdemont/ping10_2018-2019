<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 12/12/2018
 * Time: 17:15
 */

include_once ("dto_user.php");

/**
 * Class Adviser
 * @property  listCommercants
 */
class Adviser extends User
{
    private $listCommercants;


    /**
     * Adviser constructor.
     * @param $userId
     * @param $civilite
     * @param $nom
     * @param $prenom
     * @param $listCommercants
     */
    public function __construct($userId, $civilite, $nom, $prenom, $listCommercants)
    {
        parent::__construct($userId, null, null, $civilite, $nom, $prenom);
        $this->listCommercants = $listCommercants;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }


    /**
     * @return mixed
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @return mixed
     */
    public function getListCommercants()
    {
        return $this->listCommercants;
    }
}
?>