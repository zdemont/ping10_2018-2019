<?php

/**
 * Class User
 */
class User
{
    /**
     * @var
     */
    protected $userId;
    protected $profil;
    protected $password;
    protected $civilite;
    protected $nom;
    protected $prenom;

    /**
     * User constructor.
     * @param $userId
     * @param $profil
     * @param $password
     * @param $civilite
     * @param $nom
     * @param $prenom
     */
    public function __construct($userId, $profil, $password, $civilite, $nom, $prenom)
    {
        $this->userId = $userId;
        $this->profil = $profil;
        $this->password = $password;
        $this->civilite = $civilite;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getProfil()
    {
        return $this->profil;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }
}
?>