<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 09/01/2019
 * Time: 13:06
 */

/**
 * Class Reporting
 */
class Reporting
{
    /**
     * @var
     */
    protected $propClt1825;
    protected $propClt2540;
    protected $propClt4055;
    protected $propClt55Plus;

    protected $propCltPro;
    protected $propCltHomme;
    protected $propCltFemme;

    protected $propFoyer1;
    protected $propFoyer2;
    protected $propFoyer3;
    protected $propFoyer4;
    protected $propFoyer5Plus;

    protected $propCSP1;
    protected $propCSP2;
    protected $propCSP3;
    protected $propCSP4;
    protected $propCSP5;
    protected $propCSP6;
    protected $propCSP7;
    protected $propCSP8;

    /**
     * Reporting constructor.
     * @param $propClt1825
     * @param $propClt2540
     * @param $propClt4055
     * @param $propClt55Plus
     * @param $propCltPro
     * @param $propCltHomme
     * @param $propCltFemme
     * @param $propFoyer1
     * @param $propFoyer2
     * @param $propFoyer3
     * @param $propFoyer4
     * @param $propFoyer5Plus
     * @param $propCSP1
     * @param $propCSP2
     * @param $propCSP3
     * @param $propCSP4
     * @param $propCSP5
     * @param $propCSP6
     * @param $propCSP7
     * @param $propCSP8
     */
    public function __construct($propClt1825, $propClt2540, $propClt4055, $propClt55Plus, $propCltPro, $propCltHomme, $propCltFemme,
                                $propFoyer1, $propFoyer2, $propFoyer3, $propFoyer4, $propFoyer5Plus,
                                $propCSP1, $propCSP2, $propCSP3, $propCSP4, $propCSP5, $propCSP6, $propCSP7, $propCSP8)
    {
        $this->propClt1825 = $propClt1825;
        $this->propClt2540 = $propClt2540;
        $this->propClt4055 = $propClt4055;
        $this->propClt55Plus = $propClt55Plus;
        $this->propCltPro = $propCltPro;
        $this->propCltHomme = $propCltHomme;
        $this->propCltFemme = $propCltFemme;
        $this->propFoyer1 = $propFoyer1;
        $this->propFoyer2 = $propFoyer2;
        $this->propFoyer3 = $propFoyer3;
        $this->propFoyer4 = $propFoyer4;
        $this->propFoyer5Plus = $propFoyer5Plus;
        $this->propCSP1 = $propCSP1;
        $this->propCSP2 = $propCSP2;
        $this->propCSP3 = $propCSP3;
        $this->propCSP4 = $propCSP4;
        $this->propCSP5 = $propCSP5;
        $this->propCSP6 = $propCSP6;
        $this->propCSP7 = $propCSP7;
        $this->propCSP8 = $propCSP8;
    }

    /**
     * @return mixed
     */
    public function getPropClt1825()
    {
        return $this->propClt1825;
    }

    /**
     * @return mixed
     */
    public function getPropClt2540()
    {
        return $this->propClt2540;
    }

    /**
     * @return mixed
     */
    public function getPropClt4055()
    {
        return $this->propClt4055;
    }

    /**
     * @return mixed
     */
    public function getPropClt55Plus()
    {
        return $this->propClt55Plus;
    }

    /**
     * @return mixed
     */
    public function getPropCltPro()
    {
        return $this->propCltPro;
    }

    /**
     * @return mixed
     */
    public function getPropCltHomme()
    {
        return $this->propCltHomme;
    }

    /**
     * @return mixed
     */
    public function getPropCltFemme()
    {
        return $this->propCltFemme;
    }

    /**
     * @return mixed
     */
    public function getPropFoyer1()
    {
        return $this->propFoyer1;
    }

    /**
     * @return mixed
     */
    public function getPropFoyer2()
    {
        return $this->propFoyer2;
    }

    /**
     * @return mixed
     */
    public function getPropFoyer3()
    {
        return $this->propFoyer3;
    }

    /**
     * @return mixed
     */
    public function getPropFoyer4()
    {
        return $this->propFoyer4;
    }

    /**
     * @return mixed
     */
    public function getPropFoyer5Plus()
    {
        return $this->propFoyer5Plus;
    }

    /**
     * @return mixed
     */
    public function getPropCSP1()
    {
        return $this->propCSP1;
    }

    /**
     * @return mixed
     */
    public function getPropCSP2()
    {
        return $this->propCSP2;
    }

    /**
     * @return mixed
     */
    public function getPropCSP3()
    {
        return $this->propCSP3;
    }

    /**
     * @return mixed
     */
    public function getPropCSP4()
    {
        return $this->propCSP4;
    }

    /**
     * @return mixed
     */
    public function getPropCSP5()
    {
        return $this->propCSP5;
    }

    /**
     * @return mixed
     */
    public function getPropCSP6()
    {
        return $this->propCSP6;
    }

    /**
     * @return mixed
     */
    public function getPropCSP7()
    {
        return $this->propCSP7;
    }

    /**
     * @return mixed
     */
    public function getPropCSP8()
    {
        return $this->propCSP8;
    }
}
?>