<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 12/12/2018
 * Time: 21:48
 */

/**
 * Class Frequentation
 */
class Frequentation
{
    /**
     * @var
     */
    private $libelleJour;
    private $nbMeanTrans;

    /**
     * Frequentation constructor.
     * @param $libelleJour
     * @param $nbMeanTrans
     */
    function __construct($libelleJour, $nbMeanTrans)
    {
        $this->libelleJour = $libelleJour;
        $this->nbMeanTrans = $nbMeanTrans;
    }

    /**
     * @return mixed
     */
    public function getLibelleJour()
    {
        return $this->libelleJour;
    }

    /**
     * @return mixed
     */
    public function getNbMeanTrans()
    {
        return $this->nbMeanTrans;
    }

    public function jsonSerialize()
    {
        return
            [
                'jour_id'   => $this->getLibelleJour(),
                'nb__moyen_trans' => $this->getNbMeanTrans()
            ];
    }
}
?>