<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 12/12/2018
 * Time: 16:54
 */

include_once ("dto_user.php");

/**
 * Class Commercant
 * @property  listCaHebdo
 * @property  listCaMensuelle
 * @property  listCartoClient
 * @property  listFrequentation
 * @property  listReportingClient
 * @property  listReportingClientFidele
 * @property  listReportingClientGP
 */
class Commercant extends User
{
    /**
     * @var
     */
    private $numSiret;
    private $latitude;
    private $longitude;

    private $listCaHebdo;
    private $listCaMensuelle;
    private $listCartoClient;
    private $listFrequentation;
    private $listReportingClient;
    private $listReportingClientFidele;
    private $listReportingClientGP;


    /**
     * Commercant constructor.
     *
     * @param $userId
     * @param $civilite
     * @param $nom
     * @param $prenom
     * @param $numSiret
     * @param $latitude
     * @param $longitude
     *
     *
     * @param $listCaHebdo
     * @param $listCaMensuelle
     * @param $listCartoClient
     * @param $listFrequentation
     * @param $listReportingClient
     * @param $listReportingClientFidele
     * @param $listReportingClientGP
     */
    public function __construct($userId, $civilite, $nom, $prenom, $numSiret, $latitude, $longitude,
                                $listCaHebdo, $listCaMensuelle, $listCartoClient, $listFrequentation,
                                $listReportingClient, $listReportingClientFidele, $listReportingClientGP )
    {
        $this->numSiret = $numSiret;
        $this->latitude = $latitude;
        $this->longitude = $longitude;

        parent::__construct($userId,null ,null, $civilite, $nom, $prenom);

        $this->listCaHebdo = $listCaHebdo;
        $this->listCaMensuelle = $listCaMensuelle;
        $this->listCartoClient = $listCartoClient;
        $this->listFrequentation = $listFrequentation;
        $this->listReportingClient = $listReportingClient;
        $this->listReportingClientFidele = $listReportingClientFidele;
        $this->listReportingClientGP = $listReportingClientGP;

    }

    /**
     * @return mixed
     */
    public function getNumSiret()
    {
        return $this->numSiret;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }


    /**
     * @return mixed
     */
    public function getListCaHebdo()
    {
        return $this->listCaHebdo;
    }

    /**
     * @return mixed
     */
    public function getListCaMensuelle()
    {
        return $this->listCaMensuelle;
    }

    /**
     * @return mixed
     */
    public function getListCartoClient()
    {
        return $this->listCartoClient;
    }

    /**
     * @return mixed
     */
    public function getListFrequentation()
    {
        return $this->listFrequentation;
    }

    /**
     * @return mixed
     */
    public function getListReportingClient()
    {
        return $this->listReportingClient;
    }

    /**
     * @return mixed
     */
    public function getListReportingClientFidele()
    {
        return $this->listReportingClientFidele;
    }

    /**
     * @return mixed
     */
    public function getListReportingClientGP()
    {
        return $this->listReportingClientGP;
    }
}
?>