<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 12/12/2018
 * Time: 22:48
 */

/**
 * Class CartoClient
 */
class CartoClient
{
    /**
     * @var
     */
    private $codeIris;
    private $nbCltTotal;
    private $nbCltGp;
    private $nbCltFidele;


    /**
     * CartoClient constructor.
     * @param $codeIris
     * @param $nbCltFidele
     * @param $nbCltGp
     * @param $nbCltTotal
     */
    function __construct($codeIris, $nbCltFidele, $nbCltGp, $nbCltTotal)
    {
        $this->codeIris = $codeIris;
        $this->nbCltFidele = $nbCltFidele;
        $this->nbCltGp = $nbCltGp;
        $this->nbCltTotal = $nbCltTotal;
    }

    /**
     * @return mixed
     */
    public function getCodeIris()
    {
        return $this->codeIris;
    }

    /**
     * @return mixed
     */
    public function getNbCltTotal()
    {
        return $this->nbCltTotal;
    }

    /**
     * @return mixed
     */
    public function getNbCltGp()
    {
        return $this->nbCltGp;
    }

    /**
     * @return mixed
     */
    public function getNbCltFidele()
    {
        return $this->nbCltFidele;
    }


    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return
            [
                'code_iris'   => $this->getCodeIris(),
                'nb_cli_gp' => $this->getNbCltGp(),
                'nb_cli_fidele' => $this->getNbCltFidele(),
                'nb_cli_total' => $this->getNbCltTotal()
            ];
    }
}
?>