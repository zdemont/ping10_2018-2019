<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 09/01/2019
 * Time: 13:10
 */

include_once("dto_reporting.php");

/**
 * Class ReportingClientGP
 */
class ReportingClientGP extends Reporting
{
    /**
     * ReportingClientGP constructor.
     * @param $propClt1825
     * @param $propClt2540
     * @param $propClt4055
     * @param $propClt55Plus
     * @param $propCltPro
     * @param $propCltHomme
     * @param $propCltFemme
     * @param $propFoyer1
     * @param $propFoyer2
     * @param $propFoyer3
     * @param $propFoyer4
     * @param $propFoyer5Plus
     * @param $propCSP1
     * @param $propCSP2
     * @param $propCSP3
     * @param $propCSP4
     * @param $propCSP5
     * @param $propCSP6
     * @param $propCSP7
     * @param $propCSP8
     */
    public function __construct($propClt1825, $propClt2540, $propClt4055, $propClt55Plus, $propCltPro, $propCltHomme, $propCltFemme,
                                $propFoyer1, $propFoyer2, $propFoyer3, $propFoyer4, $propFoyer5Plus,
                                $propCSP1, $propCSP2, $propCSP3, $propCSP4, $propCSP5, $propCSP6, $propCSP7, $propCSP8)
    {
        parent::__construct($propClt1825, $propClt2540, $propClt4055, $propClt55Plus, $propCltPro, $propCltHomme, $propCltFemme,
            $propFoyer1, $propFoyer2, $propFoyer3, $propFoyer4, $propFoyer5Plus,
            $propCSP1, $propCSP2, $propCSP3, $propCSP4, $propCSP5, $propCSP6, $propCSP7, $propCSP8);
    }

    /**
     * @return mixed
     */
    public function getPropClt1825()
    {
        return $this->propClt1825;
    }

    /**
     * @return mixed
     */
    public function getPropClt2540()
    {
        return $this->propClt2540;
    }

    /**
     * @return mixed
     */
    public function getPropClt4055()
    {
        return $this->propClt4055;
    }

    /**
     * @return mixed
     */
    public function getPropClt55Plus()
    {
        return $this->propClt55Plus;
    }

    /**
     * @return mixed
     */
    public function getPropCltPro()
    {
        return $this->propCltPro;
    }

    /**
     * @return mixed
     */
    public function getPropCltHomme()
    {
        return $this->propCltHomme;
    }

    /**
     * @return mixed
     */
    public function getPropCltFemme()
    {
        return $this->propCltFemme;
    }

    /**
     * @return mixed
     */
    public function getPropFoyer1()
    {
        return $this->propFoyer1;
    }

    /**
     * @return mixed
     */
    public function getPropFoyer2()
    {
        return $this->propFoyer2;
    }

    /**
     * @return mixed
     */
    public function getPropFoyer3()
    {
        return $this->propFoyer3;
    }

    /**
     * @return mixed
     */
    public function getPropFoyer4()
    {
        return $this->propFoyer4;
    }

    /**
     * @return mixed
     */
    public function getPropFoyer5Plus()
    {
        return $this->propFoyer5Plus;
    }

    /**
     * @return mixed
     */
    public function getPropCSP1()
    {
        return $this->propCSP1;
    }

    /**
     * @return mixed
     */
    public function getPropCSP2()
    {
        return $this->propCSP2;
    }

    /**
     * @return mixed
     */
    public function getPropCSP3()
    {
        return $this->propCSP3;
    }

    /**
     * @return mixed
     */
    public function getPropCSP4()
    {
        return $this->propCSP4;
    }

    /**
     * @return mixed
     */
    public function getPropCSP5()
    {
        return $this->propCSP5;
    }

    /**
     * @return mixed
     */
    public function getPropCSP6()
    {
        return $this->propCSP6;
    }

    /**
     * @return mixed
     */
    public function getPropCSP7()
    {
        return $this->propCSP7;
    }

    /**
     * @return mixed
     */
    public function getPropCSP8()
    {
        return $this->propCSP8;
    }


    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return
            [
                'prop_cli_18_25' => $this->getPropClt1825(),
                'prop_cli_25_40' => $this->getPropClt2540(),
                'prop_cli_40_55' => $this->getPropClt4055(),
                'prop_cli_55plus' => $this->getPropClt55Plus(),
                'prop_cli_pro' => $this->getPropCltPro(),
                'prop_cli_homme' => $this->getPropCltHomme(),
                'prop_cli_femme' => $this->getPropCltFemme(),
                'prop_foyer1' => $this->getPropFoyer1(),
                'prop_foyer2' => $this->getPropFoyer2(),
                'prop_foyer3' => $this->getPropFoyer3(),
                'prop_foyer4' => $this->getPropFoyer4(),
                'prop_foyer_plus5' => $this->getPropFoyer5Plus(),
                'prop_csp1' => $this->getPropCSP1(),
                'prop_csp2' => $this->getPropCSP2(),
                'prop_csp3' => $this->getPropCSP3(),
                'prop_csp4' => $this->getPropCSP4(),
                'prop_csp5' => $this->getPropCSP5(),
                'prop_csp6' => $this->getPropCSP6(),
                'prop_csp7' => $this->getPropCSP7(),
                'prop_csp8' => $this->getPropCSP8()
            ];
    }
}
?>