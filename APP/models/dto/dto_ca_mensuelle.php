<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 12/12/2018
 * Time: 23:14
 */

/**
 * Class CAMensuelle
 */
class CAMensuelle
{
    /**
     * @var
     */
    private $moisId;
    private $montantCAMois;
    private $annne;

    /**
     * CAMensuelle constructor.
     * @param $moisId
     * @param $montantCAMois
     * @param $annee
     */
    function __construct($moisId, $montantCAMois, $annee)
    {
        $this->moisId = $moisId;
        $this->montantCAMois = $montantCAMois;
        $this->annne = $annee;
    }

    /**
     * @return mixed
     */
    public function getMoisId()
    {
        return $this->moisId;
    }

    /**
     * @return mixed
     */
    public function getMontantCAMois()
    {
        return $this->montantCAMois;
    }

    /**
     * @return mixed
     */
    public function getAnnee()
    {
        return $this->annne;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return
            [
                'mois_id'   => $this->getMoisId(),
                'montant_ca_mois' => $this->getMontantCAMois(),
                'annee' => $this->getAnnee()
            ];
    }
}
?>