<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 12/12/2018
 * Time: 17:15
 */

include_once ("dto_user.php");

/**
 * Class Admin
 */
class Admin extends User
{
    /**
     * Admin constructor.
     * @param $userId
     * @param $civilite
     * @param $nom
     * @param $prenom
     */
    function __construct($userId, $civilite ,$nom, $prenom)
    {
        parent::__construct($userId, null ,null, $civilite, $nom, $prenom);
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getConnId()
    {
        return $this->connId;
    }

    /**
     * @return mixeda
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }
}
?>