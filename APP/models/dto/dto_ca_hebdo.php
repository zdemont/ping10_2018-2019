<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 12/12/2018
 * Time: 23:07
 */

/**
 * Class CAHebdo
 */
class CAHebdo
{
    /**
     * @var
     */
    private $semId;
    private $montantCASem;
    private $annee;

    /**
     * CAHebdo constructor.
     * @param $semId
     * @param $montantCASem
     * @param $annee
     */
    function __construct($semId, $montantCASem, $annee)
    {
        $this->semId = $semId;
        $this->montantCASem = $montantCASem;
        $this->annee = $annee;

    }

    /**
     * @return mixed
     */
    public function getSemId()
    {
        return $this->semId;
    }

    /**
     * @return mixed
     */
    public function getMontantCASem()
    {
        return $this->montantCASem;
    }

    /**
     * @return mixed
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    public function jsonSerialize()
    {
        return
            [
                'sem_id'   => $this->getSemId(),
                'montant_ca_sem' => $this->getMontantCASem(),
                'annee' => $this->getAnnee()
            ];
    }
}
?>