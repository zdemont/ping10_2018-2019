<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 15/01/2019
 * Time: 11:16
 */

include_once ("dao.php");
include_once ("../models/dto/dto_carto_client.php");

/**
 * Class DAOCartoClient
 *
 * Class permettant de recupérer les données depuis la table carto_client_crt
 */
class DAOCartoClient extends DAO
{
    /**
     * Methode permettant de recuperer depuis la base les données de la carte
     *
     * @param $numSiret
     * @return array|int
     */
    public function getCartoClientDB($numSiret)
    {
        $result = [];

        // Recupere les données depuis la base
        $statement = "SELECT * FROM carto_client_crt WHERE crt_num_siret = ?";
        $stmt_result = parent::queryBind($statement, $numSiret);

        // Instanciation en objet si le nombre de ligne recupéré est supérieur à 0 dans une array
        // sinon retourne -1
        if ($stmt_result->num_rows > 0) {
            while ($data = $stmt_result->fetch_assoc()) {
                $dbObject = new CartoClient(
                    $data["crt_code_iris"],
                    $data["crt_nbr_cli_gp"],
                    $data["crt_nbr_cli_fidele"],
                    $data["crt_nbr_cli_total"]);

                array_push($result, $dbObject->jsonSerialize());
            }
        } else {
            $result = -1;
        }
        return $result;
    }
}
?>