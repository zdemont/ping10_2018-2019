<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 15/01/2019
 * Time: 13:09
 */

include_once ("dao.php");
include_once ("../models/dto/dto_reporting_client_fidele.php");

/**
 * Class DAOReportingClientFidele
 *
 * Class permettant de recupérer les données depuis la table reporting_cli_fid
 */
class DAOReportingClientFidele extends DAO
{
    /**
     * Methode permettant de recuperer depuis la base les données reporting clients fideles
     *
     * @param $numSiret
     * @return array|int
     */
    public function getReportingClientFideleDB($numSiret)
    {
        $result = [];

        // Recupere les données depuis la base
        $statement = "SELECT * FROM reporting_cli_fid WHERE fid_num_siret = ?";
        $stmt_result = parent::queryBind($statement, $numSiret);

        // Instanciation en objet si le nombre de ligne recupéré est supérieur à 0 dans une array
        // sinon retourne -1
        if ($stmt_result->num_rows > 0) {
            while ($data = $stmt_result->fetch_assoc()) {
                $dbObject = new ReportingClientFidele(
                    $data["fid_prop_clt_total"],
                    $data["fid_prop_clt_1825"],
                    $data["fid_prop_clt_2540"],
                    $data["fid_prop_clt_4055"],
                    $data["fid_prop_clt_plus55"],
                    $data["fid_prop_clt_pro"],
                    $data["fid_prop_clt_h"],
                    $data["fid_prop_clt_f"],
                    $data["fid_prop_clt_foyer1"],
                    $data["fid_prop_clt_foyer2"],
                    $data["fid_prop_clt_foyer3"],
                    $data["fid_prop_clt_foyer4"],
                    $data["fid_prop_clt_foyer_plus5"],
                    $data["fid_prop_clt_csp1"],
                    $data["fid_prop_clt_csp2"],
                    $data["fid_prop_clt_csp3"],
                    $data["fid_prop_clt_csp4"],
                    $data["fid_prop_clt_csp5"],
                    $data["fid_prop_clt_csp6"],
                    $data["fid_prop_clt_csp7"],
                    $data["fid_prop_clt_csp8"]);

                array_push($result, $dbObject->jsonSerialize());
            }
        } else {
            $result = -1;
        }
        return $result;
    }

}
?>