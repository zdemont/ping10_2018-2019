<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 15/01/2019
 * Time: 12:15
 */

include_once ("dao.php");
include_once ("../models/dto/dto_reporting_client.php");

/**
 * Class DAOReportingClient
 *
 * Class permettant de recupérer les données depuis la table commercant_cmt
 */
class DAOReportingClient extends DAO
{
    /**
     * Methode permettant de recuperer depuis la base les données de reporting client
     *
     * @param $numSiret
     * @return array|int
     */
    public function getReportingClientDB($numSiret)
    {
        $result = [];

        // Recupere les données depuis la base
        $statement = "SELECT * FROM commercant_cmt WHERE cmt_num_siret = ?";
        $stmt_result = parent::queryBind($statement, $numSiret);

        // Instanciation en objet si le nombre de ligne recupéré est supérieur à 0 dans une array
        // sinon retourne -1
        if ($stmt_result->num_rows > 0) {
            while ($data = $stmt_result->fetch_assoc()) {
                $dbObject = new ReportingClient(
                    $data["cmt_panier_moy"],
                    $data["cmt_prop_clt_1825"],
                    $data["cmt_prop_clt_2540"],
                    $data["cmt_prop_clt_4055"],
                    $data["cmt_prop_clt_plus55"],
                    $data["cmt_prop_clt_pro"],
                    $data["cmt_prop_clt_h"],
                    $data["cmt_prop_clt_f"],
                    $data["cmt_prop_clt_foyer1"],
                    $data["cmt_prop_clt_foyer2"],
                    $data["cmt_prop_clt_foyer3"],
                    $data["cmt_prop_clt_foyer4"],
                    $data["cmt_prop_clt_foyer_plus5"],
                    $data["cmt_prop_clt_csp1"],
                    $data["cmt_prop_clt_csp2"],
                    $data["cmt_prop_clt_csp3"],
                    $data["cmt_prop_clt_csp4"],
                    $data["cmt_prop_clt_csp5"],
                    $data["cmt_prop_clt_csp6"],
                    $data["cmt_prop_clt_csp7"],
                    $data["cmt_prop_clt_csp8"]);

                array_push($result, $dbObject->jsonSerialize());
            }
        } else {
            $result = -1;
        }
        return $result;
    }
}
?>