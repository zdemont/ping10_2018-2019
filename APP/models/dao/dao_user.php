<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 15/01/2019
 * Time: 13:44
 */

include_once("dao.php");
include_once("../models/metier.php");
include_once("../models/dto/dto_user.php");
include_once("../models/dto/dto_commercant.php");

/**
 * Class DAOUser
 *
 * Class permettant de recupérer les données depuis les tables user_usr et commercant_cmt
 */
class DAOUser extends DAO
{
    /**
     * Méthodes permettant de recuprer données  de l'utilisateur se connectant à l'application.
     * Instancie un user
     *
     * @param $userId
     * @return int|User
     */
    public function getUserDB($userId)
    {
        $user = 0;

        // Recupere les données depuis la base
        $statement = "SELECT usr_id, usr_pfl_id, usr_password, usr_nom, usr_prenom, usr_civilite  FROM user_usr WHERE usr_id = ?";
        $stmt_result = parent::queryBind($statement, $userId);

        // Instanciation en objet si le nombre de ligne recupéré est supérieur à 0 dans une array
        // sinon retourne -1
        if ($stmt_result->num_rows > 0) {
            while ($data = $stmt_result->fetch_assoc()) {
                $dbObject = new User(
                    $data["usr_id"],
                    $data["usr_pfl_id"],
                    $data["usr_password"],
                    $data["usr_civilite"],
                    $data["usr_nom"],
                    $data["usr_prenom"]);

                $user = $dbObject;
            }
        } else {
            $user = -1;
        }
        return $user;
    }

    /**
     * Méthodes permettant de recuprer données d'un commercant et de l'instancier
     *
     * @param $userId
     * @param $civilite
     * @param $prenom
     * @param $nom
     * @return Commercant|int
     */
    public function getCommercantDB($userId, $civilite, $prenom, $nom)
    {
        $user = 0;

        // Recupere les données depuis la base
        $statement = "SELECT  cmt_num_siret, cmt_latitude, cmt_longitude  FROM commercant_cmt WHERE cmt_usr_id = ?";
        $stmt_result = parent::queryBind($statement, $userId);

        // Instanciation en objet si le nombre de ligne recupéré est supérieur à 0 dans une array
        // sinon retourne -1
        if ($stmt_result->num_rows > 0) {
            while ($data = $stmt_result->fetch_assoc()) {
                $dbObject = new Commercant(
                    $userId,
                    $civilite,
                    $prenom,
                    $nom,
                    $data["cmt_num_siret"],
                    $data["cmt_latitude"],
                    $data["cmt_longitude"],
                    getCaHebdo($data["cmt_num_siret"]),
                    getCaMensuelle($data["cmt_num_siret"]),
                    getCartoClient($data["cmt_num_siret"]),
                    getFrequentation($data["cmt_num_siret"]),
                    getReportingClient($data["cmt_num_siret"]),
                    getReportingClientFidele($data["cmt_num_siret"]),
                    getReportingClientGP($data["cmt_num_siret"]));

                $user = $dbObject;
            }
        } else {
            $user = -1;
        }
        return $user;
    }

    /**
     * Méthode permettant de vérifier l'exitence en base d'un utilisateur
     *
     * @param $userId
     * @return bool
     */
    public function verifUserDB($userId)
    {
        // Recupere les données depuis la base
        $statement = "SELECT usr_id FROM user_usr WHERE usr_id = ?";
        $stmt_result = parent::queryBind($statement, $userId);

        //si le nombre renvoyé est différent de 1 => renvoyer false
        //sinon renvoyer true
        if ($stmt_result->num_rows != 1)
            return false;//renvoyer false si la requête ne renvoie aucune ligne
        else
            return true;//renvoyer true si la requête renvoie une ligne
    }

    /**
     * Méthode permettant de récupérer la liste de tout les commercants de l'application
     *
     * @return int
     */
    public function listCommercantsDB()
    {
        $result = [];

        // Recupere les données depuis la base
        $statement = "SELECT usr_id ,usr_nom, usr_prenom, usr_civilite, cmt_num_siret, cmt_latitude, cmt_longitude FROM user_usr INNER JOIN commercant_cmt ON usr_id = cmt_usr_id WHERE usr_pfl_id = 3 ORDER BY usr_nom ";
        $stmt_result = parent::query($statement);

        // Instanciation en objet si le nombre de ligne recupéré est supérieur à 0 dans une array
        // sinon retourne -1
        if ($stmt_result->num_rows > 0) {
            while ($data = $stmt_result->fetch_assoc()) {
                $dbObject = new Commercant(
                    $data["usr_id"],
                    $data["usr_civilite"],
                    $data["usr_nom"],
                    $data["usr_prenom"],
                    $data["cmt_num_siret"],
                    $data["cmt_latitude"],
                    $data["cmt_longitude"],
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null);

                array_push($result, $dbObject);
            }
        } else {
            $result = -1;
        }
        return $result;
    }
}
?>