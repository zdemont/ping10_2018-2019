<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 10/01/2019
 * Time: 21:39
 */

include_once ("dao.php");
include_once ("../models/dto/dto_ca_hebdo.php");

/**
 * Class DAOCAHebdo
 *
 * Class permettant de recupérer les données depuis la table ca_hebdo_chb
 */
class DAOCaHebdo extends DAO
{
    /**
     * Methode permettant de recuperer depuis la base les données de  chiffres d'affaires hebdomadaires
     *
     * @param $numSiret
     * @return array|int
     */
    public function getCaHebdoDB($numSiret)
    {
        $result = [];

        // Recupere les données depuis la base
        $statement = "SELECT * FROM ca_hebdo_chb WHERE chb_num_siret = ?";
        $stmt_result = parent::queryBind($statement, $numSiret);

        // Instanciation en objet si le nombre de ligne recupéré est supérieur à 0 dans une array
        // sinon retourne -1
        if ($stmt_result->num_rows > 0) {
            while ($data = $stmt_result->fetch_assoc()) {
                $dbObject = new CAHebdo($data["chb_sem_id"],
                    $data["chb_montant_ca_sem"],
                    $data["chb_annee"]);

                array_push($result, $dbObject->jsonSerialize());
            }
        } else {
            $result = -1;
        }
        return $result;
    }
}
?>