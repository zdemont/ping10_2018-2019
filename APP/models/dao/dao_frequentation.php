<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 15/01/2019
 * Time: 11:25
 */

include_once ("dao.php");
include_once ("../models/dto/dto_frequentation.php");

/**
 * Class DAOFrequentation
 *
 * Class permettant de recupérer les données depuis la table frequentation_frq
 */
class DAOFrequentation extends DAO
{
    /**
     * Methode permettant de recuperer depuis la base les données de frequentation de l'établissement
     *
     * @param $numSiret
     * @return array|int
     */
    public function getFrequentationDB($numSiret)
    {
        $result = [];

        // Recupere les données depuis la base
        $statement = "SELECT * FROM frequentation_frq WHERE frq_num_siret = ? ORDER BY frq_jr_id";
        $stmt_result = parent::queryBind($statement, $numSiret);

        // Instanciation en objet si le nombre de ligne recupéré est supérieur à 0 dans une array
        // sinon retourne -1
        if ($stmt_result->num_rows > 0) {
            while ($data = $stmt_result->fetch_assoc()) {
                $dbObject = new Frequentation(
                    $data["frq_jr_id"],
                    $data["frq_nbr_moyen_trans"]);

                array_push($result, $dbObject->jsonSerialize());
            }
        } else {
            $result = -1;
        }
        return $result;
    }

}
?>