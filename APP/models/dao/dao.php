<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 09/01/2019
 * Time: 22:26
 */

/**
 * Class DAO
 */
class DAO
{
    /**
     * @var string
     */
    protected $dbHost;
    protected $dbUser;
    protected $dbPass;
    protected $dbName;
    protected $conn;

    /**
     * DAO constructor.
     * @param string $dbHost
     * @param string $dbUser
     * @param string $dbPass
     * @param string $dbName
     */

    public function __construct()
    {
        // Accède au fichier de configuration
        $configFile = fopen('../models/dao/config.txt', 'r');
        for ($i=0; $i<=4; $i++){
            $ligne = fgets($configFile);
            $connexionParams [] = $ligne ;
        }
        fclose($configFile);

        $this->dbHost = substr($connexionParams[1], 0, -2);
        $this->dbUser = substr($connexionParams[2], 0, -2);
        $this->dbPass = substr($connexionParams[3], 0, -2);
        $this->dbName = $connexionParams[4];
    }


    /**
     * Make a connection with the database
     *
     * @return mysqli
     */
    private function connexionDB (){
        try {
            $conn = mysqli_connect($this->dbHost, $this->dbUser, $this->dbPass,$this->dbName);

            if (mysqli_connect_error()) {
                throw new Exception(mysqli_connect_error());
            }

        }catch (Exception $e){
            echo "Can't connect to BDD" ;
        }
        return $conn;
    }


    /**
     * Execute statement with one bind parameter
     *
     * @param $statement
     * @param $bindParam
     * @return bool|mysqli_result
     */
    public function queryBind($statement, $bindParam)
    {
        //new connection to database
        $conn = $this->connexionDB();
        try {
            // prepare and bind
            $stmt = $conn->prepare($statement);
            $stmt -> bind_param("s", $bindParam);

            //execute and store the results
            $stmt->execute();

        }catch (Exception $e){
            echo "Can't not execute the statement ";
        }

        //$conn->close();

        return $stmt->get_result();
    }

    public function query($statement)
    {
        //new connection to database
        $conn = $this->connexionDB();
        try {
            // prepare and bind
            $stmt = $conn->prepare($statement);

            //execute and store the results
            $stmt->execute();

        }catch (Exception $e){
            echo "Can't not execute the statement ";
        }

        //$conn->close();

        return $stmt->get_result();
    }
}
?>