<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 15/01/2019
 * Time: 11:08
 */

include_once ("dao.php");
include_once ("../models/dto/dto_ca_mensuelle.php");

/**
 * Class DAOCaMensuelle
 *
 * Class permettant de recupérer les données depuis la table ca_mensu_cmn
 */
class DAOCaMensuelle extends DAO
{
    /**
     * Methode permettant de recuperer depuis la base les données de chiffres d'affaires mensuelles
     *
     * @param $numSiret
     * @return array|int
     */
    public function getCaMensuelleDB($numSiret)
    {
        $result = [];

        // Recupere les données depuis la base
        $statement = "SELECT * FROM ca_mensu_cmn WHERE cmn_num_siret = ? ORDER BY cmn_mois_id";
        $stmt_result = parent::queryBind($statement, $numSiret);

        // Instanciation en objet si le nombre de ligne recupéré est supérieur à 0 dans une array
        // sinon retourne -1
        if ($stmt_result->num_rows > 0) {
            while ($data = $stmt_result->fetch_assoc()) {
                $dbObject = new CAMensuelle(
                    $data["cmn_mois_id"],
                    $data["cmn_montant_ca_mois"],
                    $data["cmn_annee"]);

                array_push($result, $dbObject->jsonSerialize());
            }
        } else {
            $result = -1;
        }
        return $result;
    }
}
?>