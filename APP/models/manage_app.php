<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 15/01/2019
 * Time: 15:12
 */

include_once ("dao/dao_user.php");

/**
 * Methode permettant d'instancier la DAO et excute la methode getUserDB
 *
 * @param $userId
 * @return int|User
 */
function getUser ($userId){
    $objet = new DAOUser();
    return $objet->getUserDB($userId);
}

/**
 * Methode permettant d'instancier la DAO et excute la methode getCommercantDB
 *
 * @param $userId
 * @param $civilite
 * @param $prenom
 * @param $nom
 * @return Commercant|int
 */
function getCommercant ($userId, $civilite, $prenom, $nom){
    $objet = new DAOUser();
    return $objet->getCommercantDB($userId, $civilite, $prenom, $nom );
}

/**
 * Methode permettant d'instancier la DAO et excute la methode verifUserDB
 *
 * @param $userId
 * @return bool
 */
function verifUser ($userId){
    $objet = new DAOUser();
    return $objet->verifUserDB($userId);
}

/**
 * Methode permettant d'instancier la DAO et excute la methode listCommercants
 *
 * @return int
 */
function listCommercants (){
    $objet = new DAOUser();
    return $objet->listCommercantsDB();
}
?>