<?php
/**
 * destruction des variables de session + destruction de la session et redirection page d'accueil
 */
if (!isset($_SESSION))
{
    session_start();
}

if (isset($_SESSION))
{
    session_unset();
    session_destroy();
}

header('location: ../views/index.php');
?>