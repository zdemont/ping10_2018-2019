<?php
/**
 *Controller permettant de déconnecter
 */

if (!isset($_SESSION))
{
    session_start();
}

if (isset($_SESSION))
{
    session_unset();
    session_destroy();
}


header('location: ../index.php');
?>