<?php
include_once ("../models/dto/dto_commercant.php");

if(!isset($_SESSION)){
    session_start();
}

$user = $_SESSION['userRetailer'];

//test
/* include_once ("../models/dao/dao_user.php");
$user_id = "retailer";
$dao = new DAOUser();
$user = $dao->getCommercantDB($user_id,"M","Prenom","Nom");
$test = "frequentation"; */

// switch permettant de choisir le traitement à effectuer en fonction du paramètre envoyé par la requête GET
switch($_GET['func']){
    case "cahebdo":
        $responseData = traitementDataCAHebdo($user);
        echo $responseData;
    break;

    case "camensuel":
        $responseData = traitementDataCAMens($user);
        echo $responseData;
    break;

    case "frequentation":
        $responseData = traitementDataFrequentation($user);
        echo $responseData;
    break;

    default:
        echo "<script>console.log(\"Error catched link_retailer_dashboard: ".$_GET["func"]." is not a handled case\")</script>";
    break;

}

function getStartAndEndDate($week, $year) {
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $ret['week_start'] = $dto->format('d/m');
    $dto->modify('+6 days');
    $ret['week_end'] = $dto->format('d/m');
    return $ret;
  }
    
// Fonction pour préparer les données à envoyer au Graphe CA Hebdo
function traitementDataCAHebdo($user){
    $resultCAHebdo=$user->getListCaHebdo();

    $annee = $resultCAHebdo[0]['annee'];

    $semaineReporting = array();
    for($i = 0; $i < count($resultCAHebdo); ++$i) {
        $startnend = getStartAndEndDate($resultCAHebdo[$i]['sem_id'], $annee);
        array_push($semaineReporting,array("Du ".$startnend['week_start']." au ".$startnend['week_end'],$resultCAHebdo[$i]['montant_ca_sem']));
    }

    $semaineReporting = array("annee" => $annee, "semaineReporting" => $semaineReporting);
    return json_encode($semaineReporting);
}

// Fonction pour préparer les données à envoyer au Graphe CA Mensuel
function traitementDataCAMens($user){
    $resultCAMens=$user->getListCaMensuelle();

    $annee = $resultCAMens[0]['annee'];

    $moisReporting = array();

    $mois_lib = "";
    for($i = 0; $i < count($resultCAMens); ++$i) {
        $mois_lib = get_mois_libelle($resultCAMens[$i]['mois_id']);
        array_push($moisReporting,array($mois_lib,$resultCAMens[$i]['montant_ca_mois']));
    }

    $donneesAffichees = array("annee" => $annee, "moisReporting" => $moisReporting);
    return json_encode($donneesAffichees);
}

// Fonction pour préparer les données à envoyer au graphe des fréquentations
function traitementDataFrequentation($user){
    $result=$user->getListFrequentation();
    $result_jour_ids = array_column($result,'jour_id');
    
    //test
    //print_r($result_jour_ids);

    $resultFrequentation = array();

    $jour_lib = "";
    $nb_moy_trans = "";
    $found_key=0;
    
    for($id_jour = 1; $id_jour < 8; ++$id_jour) {

        $jour_lib = get_libelle_jour($id_jour);

        $found_key = array_search($id_jour,$result_jour_ids);

        if($found_key!==false){
            $nb_moy_trans =  $result[$found_key]['nb__moyen_trans'];
            //test
            //echo "<p> found key:".$found_key."\t id_jour:".$id_jour." nbr:".$nb_moy_trans."</p>";    
        }
        else
            $nb_moy_trans =  0;

        array_push($resultFrequentation,array($jour_lib,$nb_moy_trans));
    }

    return json_encode($resultFrequentation);
}

// Fonction permettant de connaitre le libellé en français d'un mois connaissant son ID
function get_mois_libelle($mois_id){
    switch($mois_id){
        case '1': return "Janvier"; break;

        case '2': return "Février"; break;

        case '3': return "Mars"; break;

        case '4': return "Avril"; break;

        case '5': return "Mai"; break;

        case '6': return "Juin"; break;

        case '7': return "Juillet"; break;

        case '8': return "Aout"; break;

        case '9': return "Septembre"; break;

        case '10': return "Octobre"; break;

        case '11': return "Novembre"; break;

        case '12': return "Decembre"; break;

        default: echo "<script>console.log(\"mois_id doit etre compris entre 1 et 12\")</script>"; break;
   }
}

// function permettant de récupérer le libellé en français d'un jour de la semaine
function get_libelle_jour($id_jour){
    switch($id_jour){
        case '1': return "Dimanche"; break;

        case '2': return "Lundi"; break;

        case '3': return "Mardi"; break;

        case '4': return "Mercredi"; break;

        case '5': return "Jeudi"; break;

        case '6': return "Vendredi"; break;

        case '7': return "Samedi"; break;

        default: echo "<script>console.log(\"Les numéros de jours doivent être compris entre 1 et 7\")</script>";
            return null;
        break;
   }
}
?>