<?php

include_once ("../models/dto/dto_commercant.php");

if(!isset($_SESSION)){
    session_start();
}

$user = $_SESSION["userRetailer"];

switch($_GET['donnee'])
{
    case "foyerall":
        $reporting = $user->getListReportingClient();
        $prop_foyer = [];
        array_push($prop_foyer, $reporting[0]["prop_foyer1"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer2"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer3"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer4"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer_plus5"]);

        echo json_encode($prop_foyer);

    break;
    case "foyerGP":
        $reporting = $user->getListReportingClientFidele();
        $prop_foyer = [];
        array_push($prop_foyer, $reporting[0]["prop_foyer1"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer2"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer3"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer4"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer_plus5"]);

        echo json_encode($prop_foyer);
    break;
    case "foyerfidele":
        $reporting = $user->getListReportingClientGP();
        $prop_foyer = [];
        array_push($prop_foyer, $reporting[0]["prop_foyer1"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer2"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer3"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer4"]);
        array_push($prop_foyer, $reporting[0]["prop_foyer_plus5"]);

        echo json_encode($prop_foyer);
    break;
    case "Ageall":
        $reporting = $user->getListReportingClient();
        $prop_foyer = [];
        array_push($prop_foyer, $reporting[0]["prop_cli_18_25"]);
        array_push($prop_foyer, $reporting[0]["prop_cli_25_40"]);
        array_push($prop_foyer, $reporting[0]["prop_cli_40_55"]);
        array_push($prop_foyer, $reporting[0]["prop_cli_55plus"]);

        echo json_encode($prop_foyer);
    break;
    case "Agefidele":
        $reporting = $user->getListReportingClientFidele();
        $prop_foyer = [];
        array_push($prop_foyer, $reporting[0]["prop_cli_18_25"]);
        array_push($prop_foyer, $reporting[0]["prop_cli_25_40"]);
        array_push($prop_foyer, $reporting[0]["prop_cli_40_55"]);
        array_push($prop_foyer, $reporting[0]["prop_cli_55plus"]);

        echo json_encode($prop_foyer);
    break;
    case "AgeGP":
        $reporting = $user->getListReportingClientGP();
        $prop_foyer = [];
        array_push($prop_foyer, $reporting[0]["prop_cli_18_25"]);
        array_push($prop_foyer, $reporting[0]["prop_cli_25_40"]);
        array_push($prop_foyer, $reporting[0]["prop_cli_40_55"]);
        array_push($prop_foyer, $reporting[0]["prop_cli_55plus"]);

        echo json_encode($prop_foyer);
    break;
    case "hfall":
         $reporting = $user->getListReportingClient();
         $prop_hf = [];
         array_push($prop_hf, $reporting[0]["prop_cli_homme"]);
         array_push($prop_hf, $reporting[0]["prop_cli_femme"]);

         echo json_encode($prop_hf);

    break;
    case "hffidele":
        $reporting = $user->getListReportingClientFidele();
        $prop_hf = [];
        array_push($prop_hf, $reporting[0]["prop_cli_homme"]);
        array_push($prop_hf, $reporting[0]["prop_cli_femme"]);

        echo json_encode($prop_hf);
    break;
    case "hfgp":
        $reporting = $user->getListReportingClientGP();
        $prop_hf = [];
        array_push($prop_hf, $reporting[0]["prop_cli_homme"]);
        array_push($prop_hf, $reporting[0]["prop_cli_femme"]);

        echo json_encode($prop_hf);
    break;

    case "cspall":
        $reporting = $user->getListReportingClient();
        $prop_csp = [];
        array_push($prop_csp, $reporting[0]["prop_csp1"]);
        array_push($prop_csp, $reporting[0]["prop_csp2"]);
        array_push($prop_csp, $reporting[0]["prop_csp3"]);
        array_push($prop_csp, $reporting[0]["prop_csp4"]);
        array_push($prop_csp, $reporting[0]["prop_csp5"]);
        array_push($prop_csp, $reporting[0]["prop_csp6"]);
        array_push($prop_csp, $reporting[0]["prop_csp7"]);
        array_push($prop_csp, $reporting[0]["prop_csp8"]);

        echo json_encode($prop_csp);
    break;

    case "cspfidele":
        $reporting = $user->getListReportingClientFidele();
        $prop_csp = [];
        array_push($prop_csp, $reporting[0]["prop_csp1"]);
        array_push($prop_csp, $reporting[0]["prop_csp2"]);
        array_push($prop_csp, $reporting[0]["prop_csp3"]);
        array_push($prop_csp, $reporting[0]["prop_csp4"]);
        array_push($prop_csp, $reporting[0]["prop_csp5"]);
        array_push($prop_csp, $reporting[0]["prop_csp6"]);
        array_push($prop_csp, $reporting[0]["prop_csp7"]);
        array_push($prop_csp, $reporting[0]["prop_csp8"]);

        echo json_encode($prop_csp);
    break;
    case "cspgp":
        $reporting = $user->getListReportingClientGP();
        $prop_csp = [];
        array_push($prop_csp, $reporting[0]["prop_csp1"]);
        array_push($prop_csp, $reporting[0]["prop_csp2"]);
        array_push($prop_csp, $reporting[0]["prop_csp3"]);
        array_push($prop_csp, $reporting[0]["prop_csp4"]);
        array_push($prop_csp, $reporting[0]["prop_csp5"]);
        array_push($prop_csp, $reporting[0]["prop_csp6"]);
        array_push($prop_csp, $reporting[0]["prop_csp7"]);
        array_push($prop_csp, $reporting[0]["prop_csp8"]);

        echo json_encode($prop_csp);
    break;
    case "PanierMoy":
        $reporting = $user->getListReportingClient();
        echo $reporting[0]["panier_moy"];
    break;
    case "clifidele":
        $reporting = $user->getListReportingClientFidele();
        echo $reporting[0]["prop_cli_fid_total"];
    break;
    case "clipro":
        $reporting = $user->getListReportingClient();
        echo $reporting[0]["prop_cli_pro"];
    break;
    case "checkall":
        $result = $user->getListCartoClient();
        $count = 0;
        foreach($result as $value){
            $count += $value["nbr_cli_total"];
        }
        echo $count;
    break;
    case "checkfidele":
        $result = $user->getListCartoClient();
        $count = 0;
        foreach($result as $value){
            $count += $value["nbr_cli_fidele"];
        }
        echo $count;
    break;
    case "checkgp":
        $result = $user->getListCartoClient();
        $count = 0;
        foreach($result as $value){
            $count += $value["nbr_cli_gp"];
        }
        echo $count;
    break;
    default:
        echo "error";
    break;
}
        
?>