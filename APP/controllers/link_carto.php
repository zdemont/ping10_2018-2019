<?php

include_once("../models/dto/dto_commercant.php");

include_once("../models/manage_app.php");

if(!isset($_SESSION)){
    session_start();
}

$user = $_SESSION['userRetailer'];

$iris_client = $user->getListCartoClient();
$lat = $user -> getLatitude();
$lng = $user -> getLongitude();
$nbrclient = array();


switch ($_GET['type']) {
    case "total":
        foreach ($iris_client as $object) {
            array_push($nbrclient, $object["nb_cli_total"]);
        }
        break;
    case "fidele":
        foreach ($iris_client as $object) {
            array_push($nbrclient, $object["nb_cli_fidele"]);
        }
        break;
    case "grosP":
        foreach ($iris_client as $object) {
            array_push($nbrclient, $object["nb_cli_gp"]);
        }
        break;
}

$max = max($nbrclient);
$arrary = array("max" => $max, "json" => $user->getListCartoClient(), "latitude"=>$lat, "longitude"=>$lng);
echo json_encode($arrary);

?>