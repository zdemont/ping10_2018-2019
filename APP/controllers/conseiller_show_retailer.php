<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 16/01/2019
 * Time: 23:11
 */

include_once ("../models/manage_app.php");

$getUser = getUser($_GET["id"]);

$commercant = getCommercant($getUser->getUserId(),
                            $getUser->getCivilite(),
                            $getUser->getPrenom(),
                            $getUser->getNom());

session_start();

$_SESSION['userRetailer']=$commercant;

header('location:../views/retailer_start.php');
?>