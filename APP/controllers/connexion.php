<?php
/**
 * Created by PhpStorm.
 * User: Angels
 * Date: 15/01/2019
 * Time: 15:12
 */

include_once ("../models/manage_app.php");
include_once ("../models/dto/dto_user.php");
include_once ("../models/dto/dto_adviser.php");
include_once ("../models/dto/dto_commercant.php");





//if get post from connexion.php do connexion processing
if(isset($_POST['connexion'])){

    //transform all character on HTML form 
    $id = htmlentities($_POST['id']);
    $password = htmlentities($_POST['password']);


    $msg_error=[];
    $autorisationConnection = true;
    $user = getUser($id);

    //Vérification des champs vides 
    if(empty($_POST['id']) || empty($_POST['password'])){
        $msg_error[] = 'error connexion !';
        $autorisationConnection=false;
    }

    //Vérification de l'existance de l'utilisateur
    if (is_numeric($user)){
        if ($user == -1){
            $msg_error[] = 'user or password are wrong !';
            $autorisationConnection=false;
        }
    }else{
        $verifPassword = password_verify($password, $user->getPassword());
    }

    //mot de passe généré avec cette fonction 
    //$pass = password_hash('123',PASSWORD_DEFAULT);

    //Verification du mot de passe 
    if ($verifPassword == false){
        $msg_error[] = 'user or password are wrong !';
        $autorisationConnection=false;
    }
    
    //Connection
    if ($autorisationConnection == true){
        if (isset($_SESSION))
        {
            session_unset();
            session_destroy();
        }

        //redirection en fonction du statut
        if($user->getProfil() == 2){
            if (!isset($_SESSION))
            {
                session_start();
            }

            $userAdviser = new Adviser(
                $user->getUserId(),
                $user->getCivilite(),
                $user->getNom(),
                $user->getPrenom(),
                listCommercants());

            $_SESSION['userAdviser'] = $userAdviser;
            header('location: ../views/conseiller_dashboard.php');


        }elseif($user->getProfil() == 3){
            if (!isset($_SESSION))
            {
                session_start();
            }

            $userRetailer = getCommercant(
                        $user->getUserId(),
                        $user->getCivilite(),
                        $user->getNom(),
                        $user->getPrenom());

            $_SESSION['userRetailer'] = $userRetailer;
            header('location: ../views/retailer_start.php');
        }else{
            $autorisationConnection=false;
        }

    }else {
        if (!isset($_SESSION))
        {
            session_start();
        }
        $_SESSION['error'] = 1;
        header('location: ../views/index.php');
    }
}
?>
